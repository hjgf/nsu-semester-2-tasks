#include <stdio.h>
#include <stdlib.h>

#define NO_MEMORY 1
#define ERROR_READING_FILE 2
#define ERROR_OPENING_FILE 3

//#define DEBUG


enum color { white, gray, black };

//List realistion

typedef struct list
{
	int destVertex; //destination vertex
	struct list* next;
} List;

List* createList(int vertex)
{
	List* list = (List*)malloc(sizeof(List));
	if (NULL == list)
	{
		return NULL;
	}
	list->next = NULL;
	list->destVertex = vertex;
	return list;
}

void deleteList(List* list)
{
	if (NULL == list)
	{
		return;
	}
	deleteList(list->next);
	free(list);
}

//Returns pointer to new node or NULL if no memory
//Doesn't save order
//Must be checked if list isn't NULL before using
List* addToList(List** list, int vertex)
{
	while (NULL != (*list)->next)
	{
		list = &(*list)->next;
	}
	List* ptr = createList(vertex);
	return (*list)->next = ptr;
}

//For debug
void printList(FILE* output, List* head)
{
	if (NULL == head)
	{
		fputc('X', output);
		return;
	}
	fprintf(output, "%d -> ", head->destVertex + 1);
	printList(output, head->next);
}

void depthFirstSearch(List** vertex, int* vertexColor, int startingPoint,
	int* vertexComponent, int componentNum)
{
	List* tmp = vertex[startingPoint];
	vertexColor[startingPoint] = gray;
	vertexComponent[startingPoint] = componentNum;
	while (NULL != tmp)
	{
		if (white == vertexColor[tmp->destVertex])
		{
			depthFirstSearch(vertex, vertexColor, tmp->destVertex,
				vertexComponent, componentNum);
		}
		tmp = tmp->next;
	}
	vertexComponent[startingPoint] = componentNum;
	vertexColor[startingPoint] = black;

}

int main()
{
	FILE* input = fopen("input.txt", "r");
	if (NULL == input)
	{
		fprintf(stderr, "Failed to open input file");
		return ERROR_OPENING_FILE;
	}

	FILE* output = fopen("output.txt", "w");
	if (NULL == output)
	{
		fprintf(stderr, "Failed to open output file");
		return ERROR_OPENING_FILE;
	}

	int countVertex;
	int countRibs;
	if (2 != fscanf(input, "%d %d", &countVertex, &countRibs))
	{
		fprintf(stderr, "Error reading file");
		return ERROR_READING_FILE;
	}

	List** vertex; // Array of lists with ribs
	if (NULL == (vertex = (List**)calloc(sizeof(List*), countVertex)))
	{
		fprintf(stderr, "Not enough memory");
		return NO_MEMORY;
	}

	//Filling array of adjacency lists
	for (int i = 0; i < countRibs; i++)
	{
		int vertex1, vertex2;
		if (2 != fscanf(input, "%d %d", &vertex1, &vertex2))
		{
			fprintf(stderr, "Error reading file");
			return ERROR_READING_FILE;
		}
		vertex1--; 
		vertex2--;
		//Adding rib from vertex1 to vertex2
		if (NULL == vertex[vertex1])
		{
			if (NULL == (vertex[vertex1] = createList(vertex2)))
			{
				fprintf(stderr, "Not enough memory");
				return NO_MEMORY;
			}
		}
		else
		{
			if (NULL == addToList(&(vertex[vertex1]), vertex2))
			{
				fprintf(stderr, "Not enough memory");
				return NO_MEMORY;
			}
		}
		//Adding rib from vertex2 to vertex1
		if (NULL == vertex[vertex2])
		{
			if (NULL == (vertex[vertex2] = createList(vertex1)))
			{
				fprintf(stderr, "Not enough memory");
				return NO_MEMORY;
			}
		}
		else
		{
			if (NULL == addToList(&(vertex[vertex2]), vertex1))
			{
				fprintf(stderr, "Not enough memory");
				return NO_MEMORY;
			}
		}
	}

	int* vertexColor; 
	if (NULL == (vertexColor = (int*)malloc(sizeof(int) * countVertex)))
	{
		fprintf(stderr, "Not enough memory");
		return NO_MEMORY;
	}
	for (int i = 0; i < countVertex; i++)
	{
		vertexColor[i] = white;
	}

	//components of corresponding vertex
	int* vertexComponent;
	if (NULL == (vertexComponent = (int*)calloc(sizeof(int), countVertex)))
	{
		fprintf(stderr, "Not enough memory");
		return NO_MEMORY;
	}

	int numComponent = 0;
	for (int i = 0; i < countVertex; i++)
	{
		if (0 == vertexComponent[i])
		{
			numComponent++;
			depthFirstSearch(vertex, vertexColor, i, vertexComponent, numComponent);
			
		}
	}

#ifdef DEBUG
	for (int i = 0; i < countVertex; i++)
	{
		fprintf(output, "Vertex %d: ", i + 1);
		switch (vertexColor[i])
		{
			case white: fprintf(output, "(white)"); break;
			case gray: fprintf(output, "(gray)"); break;
			case black: fprintf(output, "(black)"); break;
		}
		fprintf(output, " comp %d\n", vertexComponent[i]);

		printList(output, vertex[i]);
		fprintf(output, "\n");
	}
#endif // DEBUG

	fprintf(output, "%d\n", numComponent);
	for (int i = 0; i < countVertex; i++)
	{
		fprintf(output, "%d ", vertexComponent[i]);
	}

	return 0;
}
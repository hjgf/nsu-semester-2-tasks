#include<stdio.h>

int getMaxLengthOneSequence(int num)
{
	int counter = 0;
	int maxLength = 0;
	for (int i = 0; i < sizeof(int)* 8; i++)
	{
		while (0 != (num & (1 << i)) && i < sizeof(int)* 8)
		{
			counter++;
			i++;
		}
		if (counter > maxLength)
		{
			maxLength = counter;
		}
		counter = 0;
	}
	return maxLength;
}

int main()
{
	FILE* input = fopen("input.txt", "r");
	if (NULL == input)
	{
		fprintf(stderr, "Failed to open input file");
		return 1;
	}

	FILE* output = fopen("output.txt", "w");
	if (NULL == output)
	{
		fprintf(stderr, "Failed to open output file");
		return 1;
	}

	int num;

	if (0 == fscanf(input, "%d", &num))
	{
		fprintf(stderr, "Error reading from file");
		return 1;
	}
	fprintf(output, "%d", getMaxLengthOneSequence(num));
}
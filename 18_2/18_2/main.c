#include <stdio.h>

#define MAX_MASK 0x80000000

unsigned int binaryReverse(int num)
{
	unsigned int mask1;
	unsigned int mask2;
	for (int i = 0; i < sizeof(int)* 8/2; i++)
	{
		mask1 = 1 << i;
		mask2 = MAX_MASK >> i;
		if ((((num & mask1) != 0) && ((num & mask2) != 0)) 
			|| (((num & mask1) == 0) && ((num & mask2) == 0)))
		{
			continue;
		}
		num ^= mask1;
		num ^= mask2;
	}
	return num;
}

void binaryPrint(FILE* output, int num)
{
	for (int i = 0; i < sizeof(int)* 8; i++)
	{
		if (0 == (num & (0x80000000 >> i)))
		{
			fputc('0', output);
		}
		else
		{
			fputc('1', output);
		}
	}
	fputc('\n', output);
	return;
}

int main()
{
	FILE* input = fopen("input.txt", "r");
	if (NULL == input)
	{
		fprintf(stderr, "Failed to open input file");
		return 1;
	}

	FILE* output = fopen("output.txt", "w");
	if (NULL == output)
	{
		fprintf(stderr, "Failed to open output file");
		return 1;
	}

	int num;

	if (0 == fscanf(input, "%d", &num))
	{
		fprintf(stderr, "Error reading from file");
		return 1;
	}
	binaryPrint(output, num);
	num = binaryReverse(num);
	fprintf(output, "%d\n", num);
	binaryPrint(output, num);
}
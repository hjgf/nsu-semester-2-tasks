#include <stdio.h>
#include <stdlib.h>

#define NO_MEMORY 1
#define ERROR_READING_FILE 2
#define ERROR_OPENING_FILE 3

int getMax(int a, int b)
{
	return (a >= b) ? a : b;
}

//Function writes maximal available cost to result array
//Arguments:
//	count - count of available items
//	weight - maximal available weight in the bag
//	matrix - two-dimentional array of pre-results
//	itemCost - cost of current item
//	itemWeight - weight of current item
void writeMaxCost(int count, int weight, int** matrix,
	int itemCost, int itemWeight)
{
	//Item weight is too much
	if (weight < itemWeight)
	{
		matrix[count][weight] = matrix[count - 1][weight];
		return;
	}
	//Chosing whether it worth to take item
	matrix[count][weight] = getMax(matrix[count - 1][weight],
			matrix[count - 1][weight - itemWeight] + itemCost);
	return;
}

void printArray(FILE* output, int** arr, int xlength, int ylength)
{
	for (int i = 0; i <= ylength; i++)
	{
		for (int j = 0; j <= xlength; j++)
		{
			fprintf(output, "%d ", arr[i][j]);
		}
		fprintf(output, "\n");
	}
	return;
}

int main()
{
	FILE* input = fopen("input.txt", "r");
	if (NULL == input)
	{
		fprintf(stderr, "Failed to open input file");
		return ERROR_OPENING_FILE;
	}

	FILE* output = fopen("output.txt", "w");
	if (NULL == output)
	{
		fprintf(stderr, "Failed to open output file");
		return ERROR_OPENING_FILE;
	}

	int count, summWeight;
	if (2 != fscanf(input, "%d %d", &count ,&summWeight))
	{
		fprintf(stderr, "Error reading file");
		return ERROR_READING_FILE;
	}

	//Memory allocation for matrix
	//For rows
	int** matrix;
	if (NULL == (matrix = (int**)malloc(sizeof(int*) * (count + 1))))
	{
		fprintf(stderr, "Not enough memory");
		return NO_MEMORY;
	}
	//For cols
	for (int i = 0; i < count + 1; i++)
	{
		if (NULL == (matrix[i] = (int*)malloc(sizeof(int) * (summWeight + 1))))
		{
			fprintf(stderr, "Not enough memory");
			return NO_MEMORY;
		}
	}

	//Zeroing first row and col
	for (int i = 0; i < count + 1 ; i++)
	{
		matrix[i][0] = 0;
	}
	for (int i = 0; i < summWeight + 1; i++)
	{
		matrix[0][i] = 0;
	}
	
	for (int i = 1; i <= count; i++)
	{
		int currentCost;
		int currentWeight;
		fscanf(input, "%d %d", &currentCost, &currentWeight);
		for (int j = 1; j <= summWeight; j++)
		{
			writeMaxCost(i, j, matrix, currentCost, currentWeight);
		}
	}

	//printArray(output, matrix, summWeight, count);
	fprintf(output, "%d", matrix[count][summWeight]);
	return 0;
}
#include <stdio.h>
#include <stdlib.h>

#define MAX_MASK 0x80000000

//Function sets bit in the int according to value argument
//Arguments:
//	num - int that needs to be changed
//	position - position of bit to change
//	value - bit to set into position
//Warning: this function isn't safe. No checks.
void setBit(int* num, int position, int value)
{
	unsigned int mask = MAX_MASK >> position;
	if ((0 != (mask & (*num)) && (0 == value))
		|| ((0 == (mask & (*num))) && (0 != value)))
	{
		*num ^= mask;
	}
}

int main()
{
	FILE* input = fopen("input.txt", "r");
	if (NULL == input)
	{
		fprintf(stderr, "Failed to open input file");
		return 1;
	}

	FILE* output = fopen("output.txt", "w");
	if (NULL == output)
	{
		fprintf(stderr, "Failed to open output file");
		return 1;
	}

	int n; //count of nums
	int m; //count of operations

	if (2 != fscanf(input, "%d %d", &n, &m))
	{
		fprintf(stderr, "Error reading from file");
		return 1;
	}

	int* arr = (int*)malloc(sizeof(int) * n);
	if (NULL == arr)
	{
		fprintf(stderr, "Not enough memory");
		return 1;
	}
	//Filling array
	//No check for whether the value has been read
	for (int i = 0; i < n; i++)
	{
		fscanf(input, "%d" ,&(arr[i]));
	}

	int bit;
	int position;
	//So is here
	for (int i = 0; i < m; i++)
	{
		fscanf(input, "%d", &bit);
		fscanf(input, "%d", &position);
		setBit(&(arr[position / 32]) , position % 32, bit);
	}
	for (int i = 0; i < n; i++)
	{
		fprintf(output, "%d ", arr[i]);
	}
	return 0;
}
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

//#pragma comment(linker, "/STACK:100000000000")

#define NO_MEMORY 1
#define ERROR_READING_FILE 2
#define ERROR_OPENING_FILE 3

//#define DEBUG

#define max(a,b)    (((a) > (b)) ? (a) : (b))
#define min(a,b)    (((a) < (b)) ? (a) : (b))

int isIntersecting(int ax, int ay, int bx, int by, int cx, int cy, int dx, int dy)
{
	float denominator = ((bx - ax) * (dy - cy)) - ((by - ay) * (dx - cx));
	float numerator1 = ((ay - cy) * (dx - cx)) - ((ax - cx) * (dy - cy));
	float numerator2 = ((ay - cy) * (bx - ax)) - ((ax - cx) * (by - ay));

	if (denominator == 0)
	{
		//On the single line
		if (numerator1 == 0 && numerator2 == 0)
		{
			int maxx1, maxx2, maxy1, maxy2, minx1, minx2, miny1, miny2;
			maxx1 = max(ax, bx);
			minx1 = min(ax, bx);
			maxy1 = max(ay, by);
			miny1 = min(ay, by);
			maxx2 = max(cx, dx);
			minx2 = min(cx, dx);
			maxy2 = max(cy, dy);
			miny2 = min(cy, dy);
			if (minx1 <= minx2 && minx2 <= maxx1 && miny1 <= miny2 && miny2 <= maxy1 )
			{
				return 1;
			}
			if (minx1 <= maxx2 && maxx2 <= maxx1 && miny1 <= maxy2 && maxy2 <= maxy1)
			{
				return 1;
			}
			if (minx2 <= minx1 && miny2 <= miny1 && maxx1 <= maxx2 && maxy1 <= maxy2)
			{
				return 1;
			}
			return 0;
		}
		//Parallel
		return 0;
	}

	float r = numerator1 / denominator;
	float s = numerator2 / denominator;

	return (r >= 0 && r <= 1) && (s >= 0 && s <= 1);
}

int main(void)
{
	FILE* input = fopen("input.txt", "r");
	if (NULL == input)
	{
		fprintf(stderr, "Failed to open input file");
		return ERROR_OPENING_FILE;
	}

	FILE* output = fopen("output.txt", "w");
	if (NULL == output)
	{
		fprintf(stderr, "Failed to open output file");
		return ERROR_OPENING_FILE;
	}

	int count = 1;
	while (1 == fscanf(input, "%d", &count) && count != 0)
	{

		int** coord;
		if (NULL == (coord = (int**)malloc(sizeof(int*)* count)))
		{
			fprintf(stderr, "Not enough memory");
			return NO_MEMORY;
		}
		for (int i = 0; i < count; i++)
		{
			if (NULL == (coord[i] = (int*)malloc(sizeof(int)* 4)))
			{
				fprintf(stderr, "Not enough memory");
				return NO_MEMORY;
			}
		}

		for (int i = 0; i < count; i++)
		{
			if (4 != fscanf(input, "%d %d %d %d", &coord[i][0], &coord[i][1], &coord[i][2], &coord[i][3]))
			{
				fprintf(stderr, "Error reading file");
				return ERROR_READING_FILE;

			}
		}

		int** matrix;
		if (NULL == (matrix = (int**)malloc(sizeof(int*)* count)))
		{
			fprintf(stderr, "Not enough memory");
			return NO_MEMORY;
		}
		for (int i = 0; i < count; i++)
		{
			if (NULL == (matrix[i] = (int*)calloc(count, sizeof(int))))
			{
				fprintf(stderr, "Not enough memory");
				return NO_MEMORY;
			}
		}

		//fprintf(output, "%d\n", isIntersecting(1,1,2,2,3,3,4,4));

		for (int i = 0; i < count; i++)
		{
			for (int j = 0; j < count; j++)
			{
				matrix[i][j] = isIntersecting(coord[i][0], coord[i][1], coord[i][2], coord[i][3],
					coord[j][0], coord[j][1], coord[j][2], coord[j][3]);
			}
		}

#ifdef DEBUG
		for (int i = 0; i < count; i++)
		{
			for (int j = 0; j < count; j++)
			{
				fprintf(output, "%d ", matrix[i][j]);
			}
			fputc('\n', output);
		}
#endif // DEBUG


		for (int k = 0; k < count; k++)
		{
			for (int i = 0; i < count; i++)
			{
				for (int j = 0; j < count; j++)
				{
					if (matrix[i][k] && matrix[k][j])
					{
						matrix[i][j] = 1;
					}
				}
			}
		}

		int match1 = 1;
		int match2 = 1;
		while (1)
		{
			fscanf(input, "%d %d", &match1, &match2);
			if (match1 == 0 && match2 == 0)
			{
				break;
			}
			if (matrix[match1 - 1][match2 - 1])
			{
				fprintf(output, "CONNECTED\n");
			}
			else
			{
				fprintf(output, "NOT CONNECTED\n");
			}
		}


#ifdef DEBUG
		fputc('\n', output);
		for (int i = 0; i < count; i++)
		{
			for (int j = 0; j < count; j++)
			{
				fprintf(output, "%d ", matrix[i][j]);
			}
			fputc('\n', output);
		}
		for (int i = 0; i < count; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				fprintf(output, "%d ", coord[i][j]);
			}
			fputc('\n', output);
		}

#endif // DEBUG
	}
	return 0;
}
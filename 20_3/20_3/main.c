#include <stdio.h>
#include <stdlib.h>

#define NO_MEMORY 1
#define ERROR_READING_FILE 2
#define ERROR_OPENING_FILE 3

#define MAX_FRAMES 26

//#define DEBUG

void printArray(FILE* output, char** arr, int xlength, int ylength);

//Returns 1 if colummn of matrix has already processed or it has non-0 elements
int isBadCol(char** matrix, int col, int dim)
{

	if (0 != matrix[dim][col])
	{
		return 1;
	}
	for (int i = 0; i < dim; i++)
	{
		if (0 != matrix[i][col])
		{
			return 1;
		}
	}
	return 0;
}

char** copyMatrix(char** oldMatrix, int dimention)
{
	//Allocating memory for copy
	char** newMatrix;
	if (NULL == (newMatrix = (char**)malloc(sizeof(char*)* (dimention + 1))))
	{
		fprintf(stderr, "Not enough memory");
		return NULL;
	}
	for (int i = 0; i <= dimention; i++)
	{
		if (NULL == (newMatrix[i] = (char*)calloc(sizeof(char), dimention)))
		{
			fprintf(stderr, "Not enough memory");
			return NULL;
		}
	}
	//Copying
	for (int i = 0; i <= dimention; i++)
	{
		for (int j = 0; j < dimention; j++)
		{
			newMatrix[i][j] = oldMatrix[i][j];
		}
	}
	return newMatrix;
}

void extractSequences(char** matrix, int dimention, char* symbols, int symbolsUsed, FILE* output)
{
	if (symbolsUsed >= dimention)
	{
		for (int i = 0; i < dimention; i++)
		{
			fputc(symbols[i], output);
		}
		fputc('\n', output);
		for (int i = 0; i < dimention; i++)
		{
			free(matrix[i]);
		}
		free(matrix);
		return;
	}
	char** newMatrix = NULL;
	for (int i = 0; i < dimention; i++)
	{
		if (!isBadCol(matrix, i, dimention))
		{
			newMatrix = copyMatrix(matrix, dimention);
			//Zeroing row(deleting ribs)
			for (int j = 0; j < dimention; j++)
			{
				newMatrix[i][j] = 0;
			}
			//Adding letter to res array
			symbols[symbolsUsed] = i + 'A';
			//Marking as visited
			newMatrix[dimention][i] = 1;
			extractSequences(newMatrix, dimention, symbols, symbolsUsed + 1, output);
		}
	}

}

int handleOverlay(char** adjancencyMatrix, char current, char top)
{
	int vertex2 = current - 'A';
	int vertex1 = top - 'A';
	if (top == current)
	{
		return 0;
	}
	adjancencyMatrix[vertex1][vertex2] = 1;
	return 0;
}

//Recover frame that made with char ch
void pickOutFrame(char** composedFrame, char ch, int width,
	int height, char** adjancencyMatrix)
{
	int leftBorder = width;
	int rightBorder = -1;
	int upBorder = height;
	int downBorder = -1;
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			if (leftBorder > j && ch == composedFrame[i][j])
			{
				leftBorder = j;
			}
			if (rightBorder < i && ch == composedFrame[i][j])
			{
				rightBorder = j;
			}
			if (upBorder > i && ch == composedFrame[i][j])
			{
				upBorder = i;
			}
			if (downBorder < i && ch == composedFrame[i][j])
			{
				downBorder = i;
			}
		}
	}
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			if ((i == upBorder || i == downBorder) && j <= rightBorder && j >= leftBorder)
			{
				handleOverlay(adjancencyMatrix, composedFrame[i][j], ch);
				continue;
			}
			if ((j == leftBorder || j == rightBorder) && i >= upBorder && i <= downBorder)
			{
				handleOverlay(adjancencyMatrix, composedFrame[i][j], ch);
				continue;
			}
		}
	}
}

void printArray(FILE* output, char** arr, int xlength, int ylength)
{
	for (int i = 0; i < ylength; i++)
	{
		for (int j = 0; j < xlength; j++)
		{
			fprintf(output, "%c", arr[i][j]);
		}
		fprintf(output, "\n");
	}
	return;
}

int main()
{
	FILE* input = fopen("input.txt", "r");
	if (NULL == input)
	{
		fprintf(stderr, "Failed to open input file");
		return ERROR_OPENING_FILE;
	}

	FILE* output = fopen("output.txt", "w");
	if (NULL == output)
	{
		fprintf(stderr, "Failed to open output file");
		return ERROR_OPENING_FILE;
	}

	int height;
	int width;

	while (2 == fscanf(input, "%d\n%d", &height, &width))
	{

		char** composedFrame;
		if (NULL == (composedFrame = (char**)malloc(sizeof(char*)* height)))
		{
			fprintf(stderr, "Not enough memory");
			return NO_MEMORY;
		}
		for (int i = 0; i < height; i++)
		{
			if (NULL == (composedFrame[i] = (char*)malloc(sizeof(char)* width)))
			{
				fprintf(stderr, "Not enough memory");
				return NO_MEMORY;
			}
		}

		char frameSymbols[MAX_FRAMES] = { 0 };
		int countSymbols = 0;

		/*Reading from file*/

		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				int tmp;
				if (EOF == (tmp = fgetc(input)))
				{
					fprintf(stderr, "Error readeing file");
					return ERROR_READING_FILE;
				}
				while ('\n' == tmp)
				{
					tmp = fgetc(input);
				}
				composedFrame[i][j] = tmp;
				//Check whether symbol is in array
				int isNew = 1;
				if ('.' != tmp)
				{
					for (int k = 0; k < MAX_FRAMES; k++)
					{
						if (tmp == frameSymbols[k])
						{
							isNew = 0;
							break;
						}
					}
					if (isNew)
					{
						frameSymbols[countSymbols] = tmp;
						countSymbols++;
					}
				}

			}
		}

		/*Graph building*/

		//Adjancency matrix with last row indicating of entry of col
		char** adjancencyMatrix = NULL;
		adjancencyMatrix = (char**)malloc(sizeof(char*)* (countSymbols + 1));

		for (int i = 0; i <= countSymbols; i++)
		{
			if (NULL == (adjancencyMatrix[i] = (char*)calloc(countSymbols, sizeof(char))))
			{
				fprintf(stderr, "Not enough memory");
				return NO_MEMORY;
			}
		}

		//Writing graph to matrix
		for (int i = 0; i < countSymbols; i++)
		{
			pickOutFrame(composedFrame, i + 'A', width, height, adjancencyMatrix);
		}

		//Clearing memory
		for (int i = 0; i < height; i++)
		{
			free(composedFrame[i]);
		}
		free(composedFrame);


		char symbols[50] = { 0 };
		extractSequences(adjancencyMatrix, countSymbols, symbols, 0, output);

		for (int i = 0; i < countSymbols; i++)
		{
			free(adjancencyMatrix[i]);
		}
		free(adjancencyMatrix);

	}
#ifdef DEBUG
	for (int i = 0; i < MAX_FRAMES; i++)
	{
		fprintf(output, "%c ", frameSymbols[i]);
	}
	fputc('\n', output);
	fprintf(output, "count = %d\n", countSymbols);
	printArray(output, composedFrame, width, height);
	fputc('\n', output);
	printArray(output, adjancencyMatrix, countSymbols, countSymbols + 1);
	fputc('\n', output);

#endif // DEBUG


	return 0;
}
#include <stdio.h>
#include <stdlib.h>

#define NO_MEMORY 1
#define ERROR_READING_FILE 2
#define ERROR_OPENING_FILE 3

//#define DEBUG

int getMaxCash(int** array, int ylength, int xlength)
{
	int tmp = array[0][xlength - 1];
	for (int i = 1; i < ylength; i++)
	{
		if (array[i][xlength - 1] > tmp)
		{
			tmp = array[i][xlength - 1];
		}
	}
	return tmp;
}

int getMax(int a, int b)
{
	return (a >= b) ? a : b;
}

void printArray(FILE* output, int** arr, int xlength, int ylength)
{
	for (int i = 0; i <= ylength; i++)
	{
		for (int j = 0; j <= xlength; j++)
		{
			fprintf(output, "%.2d ", arr[i][j]);
		}
		fprintf(output, "\n");
	}
	return;
}

void fillMatrix(int countPos, int maxTime, int** matrix)
{
	for (int j = 0; j <= maxTime; j++)
	{
		for (int i = 0; i <= j && i <= countPos; i++)
		{
			//Handling first row
			if (0 == i)
			{
				if (0 == j)
				{
					continue;
				}
				else
				{
					matrix[0][j] += getMax(matrix[0][j - 1], matrix[1][j - 1]);
				}
			}
			else
			{
				//Handling last row
				if (i == countPos)
				{
					matrix[i][j] += getMax(matrix[i][j - 1], matrix[i - 1][j - 1]);
				}
				//General case
				else
				{
					matrix[i][j] += getMax(getMax(matrix[i][j - 1],
						matrix[i - 1][j - 1]), matrix[i + 1][j - 1]);
				}
			}
		}
	}
}

int main()
{
	FILE* input = fopen("input.txt", "r");
	if (NULL == input)
	{
		fprintf(stderr, "Failed to open input file");
		return ERROR_OPENING_FILE;
	}

	FILE* output = fopen("output.txt", "w");
	if (NULL == output)
	{
		fprintf(stderr, "Failed to open output file");
		return ERROR_OPENING_FILE;
	}

	int countGang;
	int countPos;
	int endTime;
	if (3 != fscanf(input, "%d %d %d", &countGang, &countPos, &endTime))
	{
		fprintf(stderr, "Error reading file");
		return ERROR_READING_FILE;
	}

	//Allocating memory and reading from file
	int* time; //approach time
	if (NULL == (time = (int*)malloc(sizeof(int)* countGang)))
	{
		fprintf(stderr, "Not enough memory");
		return NO_MEMORY;
	}
	for (int i = 0; i < countGang; i++)
	{
		if (1 != fscanf(input, "%d", &(time[i])))
		{
			fprintf(stderr, "Error reading file");
			return ERROR_READING_FILE;
		}
	}

	int* cash;
	if (NULL == (cash = (int*)malloc(sizeof(int)* countGang)))
	{
		fprintf(stderr, "Not enough memory");
		return NO_MEMORY;
	}
	for (int i = 0; i < countGang; i++)
	{
		if (1 != fscanf(input, "%d", &(cash[i])))
		{
			fprintf(stderr, "Error reading file");
			return ERROR_READING_FILE;
		}
	}

	int* width;
	if (NULL == (width = (int*)malloc(sizeof(int)* countGang)))
	{
		fprintf(stderr, "Not enough memory");
		return NO_MEMORY;
	}
	for (int i = 0; i < countGang; i++)
	{
		if (1 != fscanf(input, "%d", &(width[i])))
		{
			fprintf(stderr, "Error reading file");
			return ERROR_READING_FILE;
		}
	}

	int** matrix;
	if (NULL == (matrix = (int**)malloc(sizeof(int*)* (countPos + 1))))
	{
		fprintf(stderr, "Not enough memory");
		return NO_MEMORY;
	}
	for (int i = 0; i <= countPos; i++)
	{
		if (NULL == (matrix[i] = (int*)calloc(sizeof(int), (endTime + 1))))
		{
			fprintf(stderr, "Not enough memory");
			return NO_MEMORY;
		}
	}

	//Inserting cash at the position of matrix, where they can be given
	for (int i = 0; i < countGang; i++)
	{
		if (width[i] <= countPos && time[i] >= width[i])
		{
			matrix[width[i]][time[i]] += cash[i];
		}
	}

#ifdef DEBUG
	printArray(output, matrix, endTime, countPos);
#endif // DEBUG


	fillMatrix(countPos, endTime, matrix);
	fprintf(output, "%d", getMaxCash(matrix, countPos + 1, endTime + 1));


#ifdef DEBUG
	fprintf(output, "\nApproach time\n");
	for (int i = 0; i < countGang; i++)
	{
		fprintf(output, "%d ", time[i]);
	}
	fprintf(output, "\nCash\n");
	for (int i = 0; i < countGang; i++)
	{
		fprintf(output, "%d ", cash[i]);
	}
	fprintf(output, "\nGangster width\n");
	for (int i = 0; i < countGang; i++)
	{
		fprintf(output, "%d ", width[i]);
	}
	fprintf(output, "\nMatrix:\n");
	printArray(output, matrix, endTime, countPos);
#endif // DEBUG
	return 0;
}
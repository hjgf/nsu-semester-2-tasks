#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>

//#pragma comment(linker, "/STACK:100000000000")

#define NO_MEMORY 1
#define ERROR_READING_FILE 2
#define ERROR_OPENING_FILE 3

#define NO_MEMORY_EXCEPTION {\
	fprintf(stderr, "Not enough memory"); \
	return NO_MEMORY; \
}
#define ERR_READING_FILE_EXCEPTION 	{\
	fprintf(stderr, "Error reading file"); \
	return ERROR_READING_FILE; \
}
#define ERR_OPENING_FILE_EXCEPTION 	{\
	fprintf(stderr, "Error opening file"); \
	return ERROR_OPENING_FILE; \
}

//#define DEBUG

typedef struct edge
{
	int v1;
	int v2;
	int weight;
} edge;

edge* createEdge(int v1, int v2, int weight)
{
	edge* retval;
	if (NULL == (retval = (edge*)malloc(sizeof(edge))))
	{
		return NULL;
	}
	retval->v1 = v1;
	retval->v2 = v2;
	retval->weight = weight;
	return retval;
}

#define MAXN 1000
int p[MAXN], rank[MAXN];
void makeset(int x)
{
	p[x] = x;
	rank[x] = 0;
}

int find_set(int x)
{
	if (x == p[x])
	{
		return x;
	}
	return p[x] = find_set(p[x]);
}

void unite(int x, int y)
{
	x = find_set(x);
	y = find_set(y);
	if (x == y) return;
	if (rank[x] > rank[y])
	{
		p[y] = x;
	}
	else
	{
		p[x] = y;
		if (rank[x] == rank[y])
		{
			++rank[y];
		}
	}
}


//Handler function for qsort from stdlib.h
int comapreEdges(const void* e1, const void* e2)
{
	if ((*(edge**)e1)->weight > (*(edge**)e2)->weight)
	{
		return 1;
	}
	else
	{
		if ((*(edge**)e1)->weight == (*(edge**)e2)->weight)
		{
			return 0;
		}
		return -1;
	}
}

int main(void)
{
	FILE* input = fopen("input.txt", "r");
	if (NULL == input)
		ERR_OPENING_FILE_EXCEPTION
		FILE* output = fopen("output.txt", "w");
	if (NULL == output)
		ERR_OPENING_FILE_EXCEPTION

		int count;
	if (1 != fscanf(input, "%d", &count))
		ERR_READING_FILE_EXCEPTION

		edge** rib; //Array of all possible roads between cities
	if (NULL == (rib = (edge**)malloc(count * count * sizeof(edge*))))
		NO_MEMORY_EXCEPTION

		//Arrays of coordinates of cities
		int* coordX;
	if (NULL == (coordX = (int*)malloc(count * sizeof(int))))
		NO_MEMORY_EXCEPTION
		int* coordY;
	if (NULL == (coordY = (int*)malloc(count * sizeof(int))))
		NO_MEMORY_EXCEPTION

		int x, y;
	//Reading coords of the cities
	for (int i = 0; i < count; i++)
	{
		if (2 != fscanf(input, "%d %d", &x, &y))
			ERR_READING_FILE_EXCEPTION
			coordX[i] = x;
		coordY[i] = y;
	}

	//Counting dist between cities
	int counter = 0;
	int tmpDist;
	for (int i = 0; i < count; i++)
	{
		for (int j = i + 1; j < count; j++)
		{
			//Piphagor
			tmpDist = ((coordX[i] - coordX[j]) * (coordX[i] - coordX[j])) +
				((coordY[i] - coordY[j]) * (coordY[i] - coordY[j]));
			rib[counter++] = createEdge(i, j, tmpDist);
			if (NULL == rib[counter - 1])
				NO_MEMORY_EXCEPTION
		}
	}

	//Sorting:
	qsort(rib, counter, sizeof(edge*), comapreEdges);

	//Making single sets from every city
	for (int i = 0; i < count; i++)
	{
		makeset(i);
	}

	//Reading old roads:
	int oldRoads;
	int v1;
	int v2;
	if (1 != fscanf(input, "%d", &oldRoads))
		ERR_READING_FILE_EXCEPTION
	for (int i = 0; i < oldRoads; i++)
	{
		if (2 != fscanf(input, "%d %d", &v1, &v2))
			ERR_READING_FILE_EXCEPTION
			unite(v1 - 1, v2 - 1);
	}

	//Kruskal algorithm:
	int newRoads = 0;
	for (int i = 0; i < counter; i++)
	{
		if (find_set(rib[i]->v1) != find_set(rib[i]->v2))
		{
			fprintf(output, "%d %d\n", rib[i]->v1 + 1, rib[i]->v2 + 1);
			unite(rib[i]->v1, rib[i]->v2);
		}
	}

#ifdef DEBUG
	for (int i = 0; i < count; i++)
	{
		for (int j = 0; j < i; j++)
		{
			fprintf(output, "       ");
		}
		for (int j = i + 1; j < count; j++)
		{
			fprintf(output, "(%d, %d) ", i, j);
		}
		fprintf(output, "\n");
	}
	for (int i = 0; i < count; i++)
	{
		fprintf(output, "City %d coord: (%d, %d)\n", i + 1, coordX[i], coordY[i]);
	}
	for (int i = 0; i < counter; i++)
	{
		fprintf(output, "Between (%d, %d) (edge %d):  d^2 = %d \n", rib[i]->v1 + 1, rib[i]->v2 + 1, i, rib[i]->weight);
	}
	fprintf(output, "Marks:\n");
	for (int i = 0; i < count; i++)
	{
		fprintf(output, "%d ", find_set(i));
	}
#endif // DEBUG

	free(coordX);
	free(coordY);
	for (int i = 0; i < count; i++)
	{
	free(rib[i]);
	}
	return 0;
}
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "maketree.h"
#include "input.h"
#include "test.h"

//Returns NULL if no memory
s_list * createList(int symbol, int freq)
{
	s_list* list;
	if (NULL == (list = (s_list *)malloc(sizeof(s_list))))
	{
		return NULL;
	}
	list->next = NULL;
	if (NULL == (list->tree = createTree(symbol, freq)))
	{
		return NULL;
	}
	return list;
}

//Returns NULL if no memory
s_tree * createTree(int symbol, int freq)
{
	s_tree* tree;
	if (NULL == (tree = (s_tree *)malloc(sizeof(s_tree))))
	{
		return NULL;
	}
	tree->left = NULL;
	tree->right = NULL;
	tree->symbol = symbol;
	tree->freq = freq;
	return tree;
}

void freeList(s_list * list)
{
	if (NULL == list)
	{
		return;
	}
	free(list->tree);
	free(list->next);
	free(list);
	return;
}

//Returns NULL if no memory
//Builds ordered by frequency list of trees
s_list * addToList(s_list ** head, s_tree * newNode)
{
	s_list * list;
	if (NULL == (list = createList(newNode->symbol, newNode->freq)))
	{
		return NULL;
	}
	list->tree->left = newNode->left;
	list->tree->right = newNode->right;

	while (NULL != *head && (*head)->tree->freq < newNode->freq)
	{
		head = &(*head)->next;
	}
	list->next = *head;
	*head = list;
	return list;
}

//Builds list from freq table
//Returns NULL if no memory
s_list * buildList(int * table, int * counter)
{
	s_list * retval = NULL;
	s_tree * tmp;
	*counter = 0;
	for (int i = 0; i < MAX_SYMBOLS; i++)
	{
		if (0 != table[i])
		{
			if (NULL == (tmp = createTree(i + 1, table[i])))
			{
				return NULL;
			}
			if (NULL == addToList(&retval, tmp))
			{
				return NULL;
			}
			(*counter)++;
		}
	}
	return retval;
}

//Returns NULL if no memory
//Must be more than 1 node in the list
s_tree * bulldTree(s_list ** head, int count)
{
	int newFreq = 0;
	if (1 == count)
	{
		return (*head)->tree;
	}
	s_list * node1;
	s_list * node2;
	s_tree * newTree;
	while (count > 1)
	{
		node1 = *head;
		node2 = node1->next;
		head = &(node2->next);
		newFreq = node1->tree->freq + node2->tree->freq;
		if (NULL == (newTree = createTree(-1, newFreq)))
		{
			return NULL;
		}
		newTree->left = node1->tree;
		newTree->right = node2->tree;
		if (NULL == addToList(head, newTree))
		{
			return NULL;
		}
		count--;
	}
	return (*head)->tree;
}

//int getCodes(s_tree * tree, int * )
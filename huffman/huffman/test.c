#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "maketree.h"
#include "input.h"
#include "error.h"
#include "test.h"

void printChars(FILE* input, FILE* output)
{
	int* table;
	table = getSymbolTable(input);
	for (unsigned int i = 0; i < MAX_SYMBOLS; i++)
	{
		if (0 != table[i])
		{
			fprintf(output, "%c : %d\n", i + 1, table[i]);
		}
	}
	int counter = 0;
	s_list * list = buildList(table, &counter);
	fprintf(output, "\n");
	printList(list, output);
	s_tree * tree = bulldTree(&list, counter);
	fprintf(output, "\n");
	printList(list, output);
	fprintf(output, "\n");
	printTree(tree, output);
	freeList(list);
	free(table);
}

void printList(s_list * head, FILE * output)
{
	if (NULL == head)
	{
		fprintf(output, "NULL\n");
		return;
	}
	fprintf(output, "(%c %d) -> \n", head->tree->symbol, head->tree->freq);
	printList(head->next, output);
	return;
}

void printTree(s_tree * head, FILE * output)
{
	if (NULL == head)
	{
		fprintf(output, "X");
		return;
	}
	fputc('(', output);
	if (-1 == head->symbol)
	{
		fprintf(output, "  %d|", head->freq);
	}
	else
	{
		fprintf(output, "%c %d|", head->symbol, head->freq);
	}
	printTree(head->left, output);
	fputc(',', output);
	printTree(head->right, output);
	fputc(')', output);
	return;
}
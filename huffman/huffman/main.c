#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "error.h"
#include "input.h"
#include "test.h"
#include "maketree.h"

#define DEBUG

int main(void)
{
	FILE* input = fopen("input.txt", "r");
	if (NULL == input)
	{
		fprintf(stderr, "Input file hasn't been open");
		return ERROR_OPENING_FILE;
	}
	FILE* output = fopen("output.txt", "w");
	if (NULL == input)
	{
		fprintf(stderr, "Output file hasn't been open");
		return ERROR_OPENING_FILE;
	}


#ifdef DEBUG
	printChars(input, output);
	s_list * list = NULL;
#endif // DEBUG

}
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "maketree.h"
#include "input.h"
#include "error.h"

//Returns table of symbols frequency or NULL if not enough memory
int * getSymbolTable(FILE * input)
{
	int * table;
	if (NULL == (table = (int *)calloc(MAX_SYMBOLS, sizeof(int))))
	{
		return NULL;
	}
	int tmp;
	while (EOF != (tmp = fgetc(input)))
	{
		table[tmp - 1]++;
	}
	rewind(input);
	return table;
}


#ifndef MAKETREE_H
#define MAKETREE_H

typedef struct s_tree
{
	struct s_tree * left;
	struct s_tree * right;
	int symbol;
	int freq;
} s_tree;

typedef struct s_list
{
	struct s_list * next;
	struct s_tree * tree;
} s_list;


typedef struct code
{
	char code[256];
} code;

s_list * createList(int symbol, int freq);
s_tree * createTree(int symbol, int freq);
s_list * addToList(s_list ** head, s_tree * newNode);
s_list * buildList(int * table, int * counter);
s_tree * bulldTree(s_list ** head, int count);
void freeList(s_list * list);

#endif
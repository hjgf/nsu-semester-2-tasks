#include <stdio.h>
#include <stdlib.h>

#define NO_MEMORY 1
#define ERROR_READING_FILE 2
#define ERROR_OPENING_FILE 3

void printArray(FILE* output, int** arr, int xlength, int ylength)
{
	for (int i = 0; i <= ylength; i++)
	{
		for (int j = 0; j <= xlength; j++)
		{
			fprintf(output, "%d ", arr[i][j]);
		}
		fprintf(output, "\n");
	}
	return;
}

int main()
{
	FILE* input = fopen("input.txt", "r");
	if (NULL == input)
	{
		fprintf(stderr, "Failed to open input file");
		return ERROR_OPENING_FILE;
	}

	FILE* output = fopen("output.txt", "w");
	if (NULL == output)
	{
		fprintf(stderr, "Failed to open output file");
		return ERROR_OPENING_FILE;
	}

	int count, divider;
	if (2 != fscanf(input, "%d %d", &count, &divider))
	{
		fprintf(stderr, "Error reading file");
		return ERROR_READING_FILE;
	}

	int** matrix;
	if (NULL == (matrix = (int**)malloc(sizeof(int*) * (count ))))
	{
		fprintf(stderr, "Not enough memory");
		return NO_MEMORY;
	}
	
	for (int i = 0; i < (count); i++)
	{
		if (NULL == (matrix[i] = (int*)calloc(sizeof(int), (divider + 1))))
		{
			fprintf(stderr, "Not enough memory");
			return NO_MEMORY;
		}
	}

	//Getting first remainder
	int tmp;
	if (1 != fscanf(input, "%d", &tmp))
	{
		fprintf(stderr, "Error reading file");
		return ERROR_READING_FILE;
	}
	//To make it positive adding divider
	matrix[0][(tmp % divider + divider) % divider] = 1;

	//Getting all following remainders
	for (int i = 0; i < count - 1 ; i++)
	{
		fscanf(input, "%d", &tmp);
		tmp = (tmp % divider + divider) % divider;

		for (int j = 0; j < divider; j++)
		{
			if (1 == matrix[i][j])
			{
				matrix[i + 1][(j + tmp) % divider] = 1;
				matrix[i + 1][((j - tmp) % divider + divider) % divider] = 1;
			}
		}
	}
	//printArray(output, matrix, divider - 1, count - 1);
	if (matrix[count - 1][0] == 1)
	{
		fprintf(output, "Divisible");
	}
	else
	{
		fprintf(output, "Not divisible");
	}
	return 0;
}
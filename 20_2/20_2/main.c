#include<stdio.h>
#include<stdlib.h>

#define white 0
#define gray 1
#define black 2
void DFS(int ** Matrix, int i, int j, int size)
{
	Matrix[i][j] = gray;
	if (j != 0)
	if (Matrix[i][j - 1] == white)
		DFS(Matrix, i, j - 1, size);
	if (j != (size - 1))
	if (Matrix[i][j + 1] == white)
		DFS(Matrix, i, j + 1, size);
	if (i != (size - 1))
	if (Matrix[i + 1][j] == white)
		DFS(Matrix, i + 1, j, size);
	Matrix[i][j] = black;
}


void printMatrix(FILE *f2, int ** Matrix, int size)
{
	int i, j;
	for (i = 0; i < size; i++)
	{
		for (j = 0; j < size; j++)
			fprintf(f2, "%d", Matrix[i][j]);
		fprintf(f2, "\n");
	}
}

int main()
{
	int i, j, size, ** Matrix, count = 0;
	char k;

	FILE * f1, *f2;

	f1 = fopen("input.txt", "r");
	f2 = fopen("output.txt", "w");

	fscanf(f1, "%d %d", &size);

	Matrix = (int **)malloc(size * sizeof(int*));

	for (i = 0; i < size; i++)
		Matrix[i] = (int*)malloc(size * sizeof(int));
	for (i = 0; i < size; i++)
	{
		for (j = 0; j < size; j++)
		{
			fscanf(f1, "%c", &k);
			if (' ' == k)
				Matrix[i][j] = white;
			if ('*' == k)
				Matrix[i][j] = black;
		}
		fscanf(f1, "\n");
	}
	printMatrix(f2, Matrix, size);
	fputc('\n', f2);
	j = 0;
	while (white != Matrix[0][j])
		j++;
	DFS(Matrix, 0, j, size);
	for (i = 1; i < size; i++)
	for (j = 0; j < size; j++)
	if (Matrix[i][j] == 0)
	{
		DFS(Matrix, i, j, size);
		count++;
	}

	printMatrix(f2, Matrix, size);


	fprintf(f2, "%d", count);

	return 0;
}
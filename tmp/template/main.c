#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>

//#pragma comment(linker, "/STACK:100000000000")

#define NO_MEMORY 1
#define ERROR_READING_FILE 2
#define ERROR_OPENING_FILE 3

#define NOT_ENOUGH_MEMORY 2


#define NO_MEMORY_EXCEPTION {fprintf(stderr, "Not enough memory"); return NO_MEMORY;}
#define ERR_READING_FILE_EXCEPTION 	{fprintf(stderr, "Error reading file");return ERROR_READING_FILE;}
#define ERR_OPENING_FILE_EXCEPTION 	{fprintf(stderr, "Error opening file");return ERROR_OPENING_FILE;}

#define DEBUG
#define MAX_SYMBOLS 256


typedef struct buffer
{
	int current;
	unsigned char byte;
	int nextByte; //For reading buffer
} buffer;

typedef struct s_tree
{
	struct s_tree * left;
	struct s_tree * right;
	int symbol;
	int freq;
	char code[256];
} s_tree;

buffer * initBuffer()
{
	buffer * buf = (buffer *)malloc(sizeof(buffer));
	if (NULL == buf)
	{
		return NULL;
	}
	buf->current = 0;
	buf->byte = 0;
	buf->nextByte = 0;
	return buf;
}

//Returns NULL if no memory
s_tree * createTree(int symbol, int freq)
{
	s_tree* tree;
	if (NULL == (tree = (s_tree *)malloc(sizeof(s_tree))))
	{
		return NULL;
	}
	tree->left = NULL;
	tree->right = NULL;
	tree->symbol = symbol;
	tree->freq = freq;
	memset(tree->code, 0, MAX_SYMBOLS);
	return tree;
}


//Returns EOF or bit value
int readBit(FILE * input, buffer * buf, char tail)
{
	int tmp;
	if (0 == buf->current)
	{
		tmp = fgetc(input);
		if (EOF == buf->nextByte)
		{
			return EOF;
		}
		buf->byte = (char)buf->nextByte;
		buf->nextByte = tmp;
		buf->current = 7;
		return buf->byte & 1;
	}
	//Checking for tail
	if (EOF == buf->nextByte && buf->current <= tail)
	{
		buf->current--;
		return EOF;
	}
	return (buf->byte >> (8 - buf->current--)) & 1;
}

int readChar(FILE * input, buffer * buf, char tail)
{
	int tmp;
	unsigned char byte = 0;
	for (int i = 0; i < 8; i++)
	{
		if (EOF == (tmp = readBit(input, buf, tail)))
		{
			return EOF;
		}
		byte |= (char)tmp << (7 - i);
	}
	return byte;
}

//Before using this function you should ensure that buffer has been initialyzed
s_tree * rebuildTree(FILE * input, buffer * buf, char tail)
{
	s_tree * tree = createTree(-1, 0);
	if (NULL == tree)
	{
		return NULL;
	}
	if (0 != readTree(tree, input, buf, tail))
	{
		return NULL;
	}
	return tree;
}

//Handler recursive function
int readTree(s_tree * tree, FILE * input, buffer * buf, char tail)
{
	int tmp;
	if (EOF == (tmp = readBit(input, buf, tail)))
	{
		return EOF;
	}
	if (0 == tmp)
	{
		if (EOF == (tree->symbol = readChar(input, buf, tail)))
		{
			return EOF;
		}
		return 0;
	}
	tree->symbol = -1;
	tree->left = createTree(-1, 0);
	tree->right = createTree(-1, 0);
	if (NULL == tree->left || NULL == tree->right)
	{
		return NOT_ENOUGH_MEMORY;
	}
	//Send up to recursion only general error
	if (readTree(tree->left, input, buf, tail) || readTree(tree->right, input, buf, tail))
	{
		return ERROR_READING_FILE;
	}
	return 0;
}

//May return error
int decodeLetter(FILE * input, s_tree ** tree, buffer * buf, char tail)
{
	int tmp;
	while (-1 == (*tree)->symbol)
	{
		if (EOF == (tmp = readBit(input, buf, tail)))
		{
			return EOF;
		}
		if (0 == tmp)
		{
			if (NULL == (*tree)->left)
			{
				//TODO change to corrupted data
				return ERROR_READING_FILE;
			}
			tree = &((*tree)->left);
		}
		else
		{
			if (NULL == (*tree)->right)
			{
				return ERROR_READING_FILE;
			}
			tree = &((*tree)->right);
		}

	}
	return (*tree)->symbol;
}

void printTree(s_tree * head, FILE * output)
{
	if (NULL == head)
	{
		fprintf(output, "X");
		return;
	}
	fputc('(', output);
	if (-1 == head->symbol)
	{
		fprintf(output, " ");
	}
	else
	{
		fprintf(output, "%c ", head->symbol);
	}
	printTree(head->left, output);
	fputc(',', output);
	printTree(head->right, output);
	fputc(')', output);
	return;
}

int main()
{
	FILE* input = fopen("input.txt", "rb");
	if (NULL == input)
		ERR_OPENING_FILE_EXCEPTION

	FILE* output = fopen("output.txt", "w");
	if (NULL == output)
		ERR_OPENING_FILE_EXCEPTION


	
	buffer * buf = initBuffer();
	buf->nextByte = getc(input);
	s_tree * tree = rebuildTree(input, buf, 0);
	printTree(tree, output);
	fprintf(output, "\n%c\n", decodeLetter(input, &tree, buf, 0));
	fprintf(output, "%c\n", decodeLetter(input, &tree, buf, 0));
	fprintf(output, "%d\n", decodeLetter(input, &tree, buf, 0));
	fprintf(output, "%d", 'b');
	//fprintf(output, "%d\n", readBit(input, buf, 5));
	//fprintf(output, "%c\n", readChar(input, buf, 5));
	//fprintf(output, "%c\n", readChar(input, buf, 5));
	//fprintf(output, "%d\n", readChar(input, buf, 5));
	//fprintf(output, "%d\n", readChar(input, buf, 5));
	//fprintf(output, "%d", 'b');
	/*for (int j = 0; j < 4; j++)
	{
		for (int i = 0; i < 8; i++)
		{
			fprintf(output, "%d", readBit(input, buf, 3));
		}
		fprintf(output, "\n");
	}*/

	return 0;
}
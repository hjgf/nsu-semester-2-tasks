#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

//#pragma comment(linker, "/STACK:100000000000")

#define NO_MEMORY 1
#define ERROR_READING_FILE 2
#define ERROR_OPENING_FILE 3

#define DEBUG




typedef enum direction
{
	up = 1, left = -2, down = -1, right = 2
} direction;

typedef struct List
{
	struct List* previous;
	int x;
	int y;
	int dist;
	direction dir;
} List;

typedef struct Queue
{
	struct List* last;
	struct List* first;
	int count;
} Queue;

List* createList(int x, int y, int dist, direction dir)
{
	List* tmp;
	if (NULL == (tmp = (List*)malloc(sizeof(List))))
	{
		return NULL;
	}
	tmp->x = x;
	tmp->y = y;
	tmp->dist = dist;
	tmp->previous = NULL;
	tmp->dir = dir;
	return tmp;
}

Queue* createQueue()
{
	Queue* tmp;
	if (NULL == (tmp = (Queue*)malloc(sizeof(Queue))))
	{
		return NULL;
	}
	tmp->first = NULL;
	tmp->last = NULL;
	tmp->count = 0;
	return tmp;
}

//Returns non-0 if error
int addToQueue(Queue* queue, int x, int y, int dist, direction dir)
{
	List* tmp;
	if (NULL == (tmp = createList(x, y, dist, dir)))
	{
		return 1;
	}
	if (NULL == queue->first)
	{
		queue->first = tmp;
		queue->last = tmp;
		return 0;
	}
	queue->last->previous = tmp;
	queue->last = tmp;
	queue->count++;
	return 0;
}

void printList(FILE* output, List* head)
{
	if (NULL == head)
	{
		return;
	}
	fprintf(output, "(%d; %d) dir = %d d = %d <- ", head->x, head->y, head->dir, head->dist);
	printList(output, head->previous);
	return;
}

void getFromQueue(Queue* queue, int* x, int* y, int* dist, direction* dir)
{
	*x = queue->first->x;
	*y = queue->first->y;
	*dist = queue->first->dist;
	*dir = queue->first->dir;
	List* ptr = queue->first;
	queue->first = queue->first->previous;
	free(ptr);
	queue->count--;
}

void deleteList(List* head)
{
	if (NULL == head)
	{
		return;
	}
	deleteList(head->previous);
	free(head);
}

void deleteQueue(Queue* queue)
{
	deleteList(queue->first);
	free(queue);
}

int isEmpty(Queue* queue)
{
	if (NULL == queue->first)
	{
		return 1;
	}
	return 0;
}

int rotateToDirection(direction initDir, direction destDir);
direction getDirection(int x1, int y1, int x2, int y2);


void breadthFirstSearch(int** map, Queue* queue, int height, int width, int* dist, direction* endDirection)
{
	int x, y, d, delta;
	direction dir;
	while (!isEmpty(queue))
	{
		
		getFromQueue(queue, &x, &y, &d, &dir);
		//Up
		if (y > 0)
		{
			delta = abs(rotateToDirection(dir, up)) + 1;
			if ('T' == map[y - 1][x])
			{
				*dist = min(d + delta, *dist);
				*endDirection = up;
			}
			if ('.' == map[y - 1][x])
			{
				map[y - 1][x] = d + delta;
				addToQueue(queue, x, y - 1, d + delta, up);
			}
			//trying to enlarge recursion to passed cells with lower cost

			//else
			//{
			//	if (INT_MAX != map[y - 1][x] && map[y - 1][x] < d + delta)
			//	{
			//		map[y - 1][x] = min(d + delta, map[y - 1][x]);
			//		addToQueue(queue, x, y - 1, d + delta, up);
			//	}
			//}
		}
		//Down
		if (y < height - 1)
		{
			delta = abs(rotateToDirection(dir, down)) + 1;
			if ('T' == map[y + 1][x])
			{
				*dist = min(d + delta, *dist);
				*endDirection = down;
			}
			if ('.' == map[y + 1][x])
			{
				map[y + 1][x] = d + delta;
				addToQueue(queue, x, y + 1, d + delta, down);
			}
			//else
			//{
			//	if (INT_MAX != map[y + 1][x] && map[y + 1][x] < d + delta)
			//	{
			//		map[y + 1][x] = min(d + delta, map[y + 1][x]);
			//		addToQueue(queue, x, y + 1, d + delta, down);
			//	}
			//}
		}
		//Left
		if (x > 0)
		{
			delta = abs(rotateToDirection(dir, left)) + 1;
			if ('T' == map[y][x - 1])
			{
				*dist = min(d + delta, *dist);
				*endDirection = left;
			}
			if ('.' == map[y][x - 1])
			{
				map[y][x - 1] = d + delta;
				addToQueue(queue, x - 1, y, d + delta, left);
			}
		}
		//Right
		if (x < width - 1)
		{
			delta = abs(rotateToDirection(dir, right)) + 1;
			if ('T' == map[y][x + 1])
			{
				*dist = min(d + delta, *dist);
				*endDirection = down;
			}
			if ('.' == map[y][x + 1])
			{
				map[y][x + 1] = d + delta;
				addToQueue(queue, x + 1, y, d + delta, right);
			}
		}
	}
}

//Absolutely correct
//returns count of rotaions(below zero means to the left)
int rotateToDirection(direction initDir, direction destDir)
{
	if (initDir == destDir)
	{
		return 0;
	}
	if (initDir == -destDir)
	{
		return 2;
	}
	if (initDir == right || initDir == down)
	{
		if (destDir < 0)
		{
			return -1;
		}
		return 1;
	}
	if (destDir > 0)
	{
		return -1;
	}
	return 1;
}


void findPath(int** map, int x, int y, int* newX, int* newY, int height, int width)
{
	int minX = -1;
	int minY = -1;
	int value = INT_MAX;
	if (y > 0 && ('#' != map[y - 1][x]) && (map[y - 1][x] < map[y][x]))
	{
		minX = y - 1;
		minY = x;
		value = map[y - 1][x];
	}
	if (x > 0 && ('#' != map[x - 1][y]) && (map[y][x - 1] < map[y][x]) && (value > map[y][x - 1]))
	{
		minX = y;
		minY = x - 1;
		value = map[y][x - 1];
	}
	if (x < width - 1 && ('#' != map[y][x + 1]) && (map[y][x + 1] < map[y][x]) && (value > map[y][x + 1]))
	{
		minX = y;
		minY = x + 1;
		value = map[y][x + 1];
	}
	if (y < height - 1 && ('#' != map[y + 1][x]) && (map[y + 1][x] < map[y][x]) && (value > map[y + 1][x]))
	{
		minX = y + 1;
		minY = x;
		value = map[y + 1][x];
	}
	*newX = minY;
	*newY = minX;
}

//Right
direction getDirection(int x1, int y1, int x2, int y2)
{
	if (x1 > x2)
	{
		return left;
	}
	if (y1 > y2)
	{
		return up;
	}
	if (y1 < y2)
	{
		return down;
	}
	return right;
}

int printActions(FILE* output, int rotations)
{
	switch (rotations)
	{
	case 0:
	{
			  fprintf(output, "F");
			  return 1;
	}
	case 1:
	{
			  fprintf(output, "LF");
			  return 2;
	}
	case -1:
	{
			   fprintf(output, "RF");
			   return 2;
	}
	case 2:
	{
			  fprintf(output, "RRF");
			  return 3;
	}
	}
}

int main()
{
	FILE* input = fopen("input.txt", "r");
	if (NULL == input)
	{
		fprintf(stderr, "Failed to open input file");
		return ERROR_OPENING_FILE;
	}

	FILE* output = fopen("output.txt", "w");
	if (NULL == output)
	{
		fprintf(stderr, "Failed to open output file");
		return ERROR_OPENING_FILE;
	}

	int height;
	int width;
	if (2 != fscanf(input, "%d %d", &height, &width))
	{
		fprintf(stderr, "Error reading file");
		return ERROR_READING_FILE;
	}

	int** map;
	if (NULL == (map = (int**)malloc(sizeof(int*)* height)))
	{
		fprintf(stderr, "Not enough memory");
		return NO_MEMORY;
	}
	for (int i = 0; i < height; i++)
	{
		if (NULL == (map[i] = (int*)malloc(sizeof(int)* width)))
		{
			fprintf(stderr, "Not enough memory");
			return NO_MEMORY;
		}
	}
	int xStart = -1;
	int yStart = -1;
	int xFinish = -1;
	int yFinish = -1;

	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			int tmp = fgetc(input);
			while ('\n' == tmp)
			{
				tmp = fgetc(input);
			}
			if ('S' == tmp)
			{
				xStart = j;
				yStart = i;
				tmp = 0;
			}
			if ('T' == tmp)
			{
				xFinish = j;
				yFinish = i;
			}
			if ('#' == tmp)
			{
				tmp = INT_MAX;
			}
			map[i][j] = tmp;
		}
	}

	Queue* queue = createQueue();
	addToQueue(queue, xStart, yStart, 0, up);
	int dist;
	direction endDir;
	dist = INT_MAX;
	breadthFirstSearch(map, queue, height, width, &dist, &endDir);

	int nx = xFinish;
	int ny = yFinish;
	int x, y;

	
	//For direct way
	int* arrayX = (int*)malloc(600* sizeof(int));
	int* arrayY = (int*)malloc(600* sizeof(int));
	int counter = 0;
	//--------------

	while (map[ny][nx] != 0)
	{
		arrayX[counter] = nx;
		arrayY[counter] = ny;
		counter++;
		x = nx;
		y = ny;
		findPath(map, nx, ny, &nx, &ny, height, width);
	}
	arrayX[counter] = xStart;
	arrayY[counter++] = yStart;

	direction oldDir = up;
	direction newDir;
	int rotations;

	fprintf(output, "%d\n", dist);
	for (int i = counter - 1; i > 0; i--)
	{
		newDir = getDirection(arrayX[i], arrayY[i], arrayX[i - 1], arrayY[i - 1]);
		rotations = rotateToDirection(oldDir, newDir);
		printActions(output, rotations);
		oldDir = newDir;
	}

#ifdef DEBUG
	for (int i = 0; i < counter; i++)
	{
		fprintf(output, "(%d %d) ", arrayX[i], arrayY[i]);
	}
#endif // DEBUG

	//Test area
	//int len = 2;
	//int currLen = 0;
	//int* arr = (int*)malloc(2 * sizeof(int));
	//addToArray(arr, 1, &len, currLen);
	//addToArray(arr, 2, &len, ++currLen);
	//for (int i = 0; i < 2; i++)
	//{
	//	fprintf(output, "%d ", arr[i]);
	//}

	/*
	int nx, ny;
	findPath(map, 0, 0, &nx, &ny, height, width);
	fprintf(output, "%d %d\n", nx, ny);


	fprintf(output, "%d , %d", dist, endDir);
	*/
#ifdef DEBUG
	fputc('\n', output);
	fprintf(output, "s = (%d;%d)\n", xStart, yStart);
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			if (INT_MAX == map[i][j])
			{
				fprintf(output, "   ");
				continue;
			}
			fprintf(output, "%.2d ", map[i][j]);
		}
		fputc('\n', output);
	}
	fputc('\n', output);
#endif // DEBUG


	return 0;
}
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

//#pragma comment(linker, "/STACK:100000000000")

#define NO_MEMORY 1
#define ERROR_READING_FILE 2
#define ERROR_OPENING_FILE 3

#define INFINITY 1000001

#define min(a,b)    (((a) < (b)) ? (a) : (b))

//Finding min dist in dist array
int minDist(int* dist, int* mark, int count)
{
	int min = INT_MAX;
	int minIndex = -1;

	for (int i = 0; i < count; i++)
	{
		if (0 == mark[i] && min > dist[i])
		{ 
			min = dist[i];
			minIndex = i;
		}
	}
	return minIndex;
}

int dijkstra(int** map, int* dist, int initPoint, int count)
{
	int* mark;
	if (NULL == (mark = (int*)calloc(count, sizeof(int))))
	{
		return NO_MEMORY;
	}

	for (int i = 0; i < count; i++)
	{
		dist[i] = INFINITY;
	}

	dist[initPoint - 1] = 0;

	int current;
	for (int i = 0; i < count - 1; i++)
	{
		current = minDist(dist, mark, count);
		mark[current] = 1;
		//Updating dist
		for (int j = 0; j < count; j++)
		{
			if (0 == mark[j] && -1 != map[current][j] && dist[current] != INFINITY && dist[current] + map[current][j] < dist[j])
			{
				dist[j] = dist[current] + map[current][j];
			}
		}
	}
}

int main()
{
	FILE* input = fopen("input.txt", "r");
	if (NULL == input)
	{
		fprintf(stderr, "Failed to open input file");
		return ERROR_OPENING_FILE;
	}

	FILE* output = fopen("output.txt", "w");
	if (NULL == output)
	{
		fprintf(stderr, "Failed to open output file");
		return ERROR_OPENING_FILE;
	}

	int serversCount;
	int initServer;
	int destServer;
	if (3 != fscanf(input, "%d\n%d %d", &serversCount, &initServer, &destServer))
	{
		fprintf(stderr, "Error reading file");
		return ERROR_READING_FILE;
	}

	int** map;
	if (NULL == (map = (int**)malloc(serversCount * sizeof(int*))))
	{
		fprintf(stderr, "Not enough memory");
		return NO_MEMORY;
	}
	for (int i = 0; i < serversCount; i++)
	{
		if (NULL == (map[i] = (int*)malloc(serversCount * sizeof(int))))
		{
			fprintf(stderr, "Not enough memory");
			return NO_MEMORY;
		}
	}

	for (int i = 0; i < serversCount; i++)
	{
		for (int j = 0; j < serversCount; j++)
		{
			map[i][j] = -1;
		}
	}

	int server1;
	int server2;
	int speed;
	while (3 == fscanf(input, "%d %d %d", &server1, &server2, &speed))
	{
		server1--;
		server2--;
		if (-1 != map[server1][server2])
		{
			map[server1][server2] = min(speed, map[server1][server2]);
			map[server2][server1] = min(speed, map[server2][server1]);
		}
		else
		{
			map[server1][server2] = speed;
			map[server2][server1] = speed;
		}
	}

	int* dist;
	if (NULL == (dist = (int*)malloc(serversCount * sizeof(int))))
	{
		fprintf(stderr, "Not enough memory");
		return NO_MEMORY;
	}

	dijkstra(map, dist, initServer, serversCount);
	if (INFINITY != dist[destServer - 1])
	{
		fprintf(output, "%d", dist[destServer - 1]);
	}
	else
	{
		fprintf(output, "no");
	}

	return 0;
}
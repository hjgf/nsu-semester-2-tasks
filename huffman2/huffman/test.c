#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "maketree.h"
#include "input.h"
#include "error.h"
#include "test.h"
#include "output.h"

void printList(s_list * head, FILE * output)
{
	if (NULL == head)
	{
		fprintf(output, "NULL\n");
		return;
	}
	fprintf(output, "(%c %d) -> \n", head->tree->symbol, head->tree->freq);
	printList(head->next, output);
	return;
}

void printTree(s_tree * head, FILE * output)
{
	if (NULL == head)
	{
		fprintf(output, "X");
		return;
	}
	fputc('(', output);
	if (-1 == head->symbol)
	{
		fprintf(output, "  %d \'%s\'|", head->freq, head->code);
	}
	else
	{
		fprintf(output, "%c %d \'%s\'|", head->symbol, head->freq, head->code);
	}
	printTree(head->left, output);
	fputc(',', output);
	printTree(head->right, output);
	fputc(')', output);
	return;
}
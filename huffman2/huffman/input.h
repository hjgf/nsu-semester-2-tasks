#ifndef INPUT_H
#define INPUT_H

#include <stdio.h>
#include "output.h"

#define MAX_SYMBOLS 256

int * getSymbolTable(FILE * input);
int readBit(FILE * input, buffer * buf, char tail);
int readChar(FILE * input, buffer * buf, char tail);
s_tree * rebuildTree(FILE * input, buffer * buf, char tail);
//Handler recursive function
int readTree(s_tree * tree, FILE * input, buffer * buf, char tail);
//Returns error
int decodeLetter(FILE * input, s_tree ** tree, buffer * buf, char tail);


#endif
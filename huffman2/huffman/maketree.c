#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "maketree.h"
#include "error.h"
#include "input.h"
#include "test.h"

//Returns NULL if no memory
s_list * createList(int symbol, int freq)
{
	s_list* list;
	if (NULL == (list = (s_list *)malloc(sizeof(s_list))))
	{
		return NULL;
	}
	list->next = NULL;
	if (NULL == (list->tree = createTree(symbol, freq)))
	{
		return NULL;
	}
	return list;
}

//Returns NULL if no memory
s_tree * createTree(int symbol, int freq)
{
	s_tree* tree;
	if (NULL == (tree = (s_tree *)malloc(sizeof(s_tree))))
	{
		return NULL;
	}
	tree->left = NULL;
	tree->right = NULL;
	tree->symbol = symbol;
	tree->freq = freq;
	memset(tree->code, 0, MAX_SYMBOLS);
	return tree;
}

void freeList(s_list * list)
{
	if (NULL == list)
	{
		return;
	}
	free(list->tree);
	free(list->next);
	free(list);
	return;
}

void freeTree(s_tree * tree)
{
	if (NULL == tree)
	{
		return;
	}
	s_tree * tmp = tree;
	freeTree(tree->left);
	freeTree(tree->right);
	free(tmp);
	return;
}

//Returns NULL if no memory
//Builds ordered by frequency list of trees
s_list * addToList(s_list ** head, s_tree * newNode)
{
	s_list * list;
	if (NULL == (list = createList(newNode->symbol, newNode->freq)))
	{
		return NULL;
	}
	list->tree->left = newNode->left;
	list->tree->right = newNode->right;

	while (NULL != *head && (*head)->tree->freq < newNode->freq)
	{
		head = &(*head)->next;
	}
	list->next = *head;
	*head = list;
	return list;
}

//Builds list from freq table
//Returns NULL if no memory
s_list * buildList(int * table, int * counter)
{
	s_list * retval = NULL;
	s_tree * tmp;
	*counter = 0;
	for (int i = 0; i < MAX_SYMBOLS; i++)
	{
		if (0 != table[i])
		{
			if (NULL == (tmp = createTree(i, table[i])))
			{
				return NULL;
			}
			if (NULL == addToList(&retval, tmp))
			{
				return NULL;
			}
			(*counter)++;
		}
	}
	return retval;
}

//Returns NULL if no memory
//Must be more than 1 node in the list
s_tree * bulldTree(s_list ** head, int count)
{
	int newFreq = 0;
	if (1 == count)
	{
		return (*head)->tree;
	}
	s_list * node1;
	s_list * node2;
	s_tree * newTree;
	while (count > 1)
	{
		node1 = *head;
		node2 = node1->next;
		head = &(node2->next);
		newFreq = node1->tree->freq + node2->tree->freq;
		if (NULL == (newTree = createTree(-1, newFreq)))
		{
			return NULL;
		}
		newTree->left = node1->tree;
		newTree->right = node2->tree;
		if (NULL == addToList(head, newTree))
		{
			return NULL;
		}
		count--;
	}
	return (*head)->tree;
}

void calcCodes(s_tree * tree)
{
	if (NULL != tree->left)
	{
		strcpy(tree->left->code, tree->code);
		strcat(tree->left->code, "0");
		calcCodes(tree->left);
	}
	if (NULL != tree->right)
	{
		strcpy(tree->right->code, tree->code);
		strcat(tree->right->code, "1");
		calcCodes(tree->right);
	}
}

//NULL if no memory
//Returns array of strings(representation of codes)
//If there is no appropirating symbol in the text, array will have NULL in this place
char ** extractCodes(s_tree * tree)
{
	char ** retArr = (char **)calloc(MAX_SYMBOLS, sizeof(char*));
	if (NULL == retArr)
	{
		return NULL;
	}
	if (recursiveExtract(tree, retArr))
	{
		return NULL;
	}
	return retArr;
}

//Handler function for extract codes
//Returns 0 if OK, error code if smth wrong
int recursiveExtract(s_tree * tree, char ** array)
{
	if (-1 != tree->symbol)
	{
		array[tree->symbol] = (char *)malloc(MAX_SYMBOLS * sizeof(char));
		if (NULL == array[tree->symbol])
		{
			return NOT_ENOUGH_MEMORY;
		}
		strcpy(array[tree->symbol], tree->code);
		return 0;
	}
	if (recursiveExtract(tree->right, array) || recursiveExtract(tree->left, array))
	{
		return NOT_ENOUGH_MEMORY;
	}
	return 0;
}
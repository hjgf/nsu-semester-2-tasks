#ifndef TEST_H
#define TEST_H

#include<stdio.h>
#include"maketree.h"

void printList(s_list * head, FILE * output);
void printTree(s_tree * head, FILE * output);

#endif
#ifndef _OUTPUT_H
#define _OUTPUT_H

#include "maketree.h"
#include <stdio.h>

typedef struct buffer
{
	int current; //Current bit for writing buffer and left bits for reading buffer
	unsigned char byte;
	int nextByte; //For reading buffer
	int totally; //Total count of writed bytes
} buffer;

void writeBit(int bit, FILE * output, buffer * buf);
void writeChar(char ch, FILE * output, buffer * buf);
int finishWriting(FILE * output, buffer * buf);
void writeTree(s_tree * tree, FILE * output, buffer * buf);
buffer * initBuffer();
void writeBinarySequence(char * sequence, FILE * output, buffer * buf);
int zip(FILE * input, FILE * output, s_tree * tree, char ** code, buffer * buf);

#endif
#include <stdio.h>
#include <stdlib.h>
#include "output.h"
#include "maketree.h"
#include "error.h"

buffer * initBuffer()
{
	buffer * buf = (buffer *)malloc(sizeof(buffer));
	if (NULL == buf)
	{
		return NULL;
	}
	buf->current = 0;
	buf->byte = 0;
	buf->nextByte = 0;
	buf->totally = 0;
	return buf;
}

//Doesn't check for correcy
void writeBit(int bit, FILE * output, buffer * buf)
{
	if (bit)
	{
		buf->byte |= (1 << buf->current);
	}
	buf->current++;
	if (8 == buf->current)
	{
		fwrite(&(buf->byte), 1, 1, output);
		buf->current = 0;
		buf->byte = 0;
		buf->totally++;
	}
}

void writeChar(char ch, FILE * output, buffer * buf)
{
	for (int i = 0; i < 8; i++)
	{
		writeBit((128 >> i) & (unsigned char)ch, output, buf);
	}
}

//Flushing buffer before the end of writing
int finishWriting(FILE * output, buffer * buf)
{
	int count = 0;
	while (0 != buf->current)
	{
		writeBit(0, output, buf);
		count++;
	}
	return count;
}

void writeTree(s_tree * tree, FILE * output, buffer * buf)
{
	if (NULL == tree->left)
	{
		writeBit(0, output, buf);
		writeChar((char)tree->symbol, output, buf);
		return;
	}
	writeBit(1, output, buf);
	writeTree(tree->left, output, buf);
	writeTree(tree->right, output, buf);
}

void writeBinarySequence(char * sequence, FILE * output, buffer * buf)
{
	while (0 != *sequence)
	{
		writeBit(*sequence - '0', output, buf);
		sequence++;
	}
}

//Returns error code or 0 if OK
int zip(FILE * input, FILE * output, s_tree * tree, char ** code, buffer * buf)
{
	int tmp;
	fseek(output, 1, SEEK_SET); //Left 1 byte to write count of free bites at the end
	if (NULL == buf)
	{
		return NOT_ENOUGH_MEMORY;
	}
	writeTree(tree, output, buf);
	while (EOF != (tmp = fgetc(input)))
	{
		writeBinarySequence(code[tmp], output, buf);
	}
	tmp = finishWriting(output, buf);
	rewind(output);
	fputc((char)tmp, output);
	return 0;
}
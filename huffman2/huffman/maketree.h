#ifndef MAKETREE_H
#define MAKETREE_H

typedef struct s_tree
{
	struct s_tree * left;
	struct s_tree * right;
	int symbol;
	int freq;
	char code[256];
} s_tree;

typedef struct s_list
{
	struct s_list * next;
	struct s_tree * tree;
} s_list;


s_list * createList(int symbol, int freq);
s_tree * createTree(int symbol, int freq);
s_list * addToList(s_list ** head, s_tree * newNode);
s_list * buildList(int * table, int * counter);
s_tree * bulldTree(s_list ** head, int count);
void freeList(s_list * list);
void calcCodes(s_tree * tree);
char ** extractCodes(s_tree * tree);
int recursiveExtract(s_tree * tree, char ** array);
void freeTree(s_tree * tree);

#endif
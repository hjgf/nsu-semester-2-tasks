#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "maketree.h"
#include "input.h"
#include "error.h"
#include "output.h"

//Returns table of symbols frequency or NULL if not enough memory
int * getSymbolTable(FILE * input)
{
	int * table;
	if (NULL == (table = (int *)calloc(MAX_SYMBOLS, sizeof(int))))
	{
		return NULL;
	}
	int tmp;
	while (EOF != (tmp = fgetc(input)))
	{
		table[tmp]++;
	}
	rewind(input);
	return table;
}

//Before using this function you should ensure that buffer has been initialyzed
s_tree * rebuildTree(FILE * input, buffer * buf, char tail)
{
	s_tree * tree = createTree(-1, 0);
	if (NULL == tree)
	{
		return NULL;
	}
	if (0 != readTree(tree, input, buf, tail))
	{
		return NULL;
	}
	return tree;
}

//Handler recursive function
int readTree(s_tree * tree, FILE * input, buffer * buf, char tail)
{
	int tmp;
	if (EOF == (tmp = readBit(input, buf, tail)))
	{
		return EOF;
	}
	if (0 == tmp)
	{
		if (EOF == (tree->symbol = readChar(input, buf, tail)))
		{
			return EOF;
		}
		return 0;
	}
	tree->symbol = -1;
	tree->left = createTree(-1, 0);
	tree->right = createTree(-1, 0);
	if (NULL == tree->left || NULL == tree->right)
	{
		return NOT_ENOUGH_MEMORY;
	}
	//Send up to recursion only general error
	if (readTree(tree->left, input, buf, tail) || readTree(tree->right, input, buf, tail))
	{
		return ERROR_READING_FILE;
	}
	return 0;
}


//Returns EOF or bit value
int readBit(FILE * input, buffer * buf, char tail)
{
	int tmp;
	if (0 == buf->current)
	{
		tmp = fgetc(input);
		if (EOF == buf->nextByte)
		{
			return EOF;
		}
		buf->byte = (char)buf->nextByte;
		buf->nextByte = tmp;
		buf->current = 7;
		return buf->byte & 1;
	}
	//Checking for tail
	if (EOF == buf->nextByte && buf->current <= tail)
	{
		buf->current--;
		return EOF;
	}
	return (buf->byte >> (8 - buf->current--)) & 1;
}


int readChar(FILE * input, buffer * buf, char tail)
{
	int tmp;
	unsigned char byte = 0;
	for (int i = 0; i < 8; i++)
	{
		if (EOF == (tmp = readBit(input, buf, tail)))
		{
			return EOF;
		}
		byte |= (char)tmp << (7 - i);
	}
	return byte;
}

//May return error
//decoding symbol from input file
int decodeLetter(FILE * input, s_tree ** tree, buffer * buf, char tail)
{
	int tmp;
	while (-1 == (*tree)->symbol)
	{
		if (EOF == (tmp = readBit(input, buf, tail)))
		{
			return EOF;
		}
		if (0 == tmp)
		{
			if (NULL == (*tree)->left)
			{
				//TODO change to corrupted data
				return ERROR_READING_FILE;
			}
			tree = &((*tree)->left);
		}
		else
		{
			if (NULL == (*tree)->right)
			{
				return ERROR_READING_FILE;
			}
			tree = &((*tree)->right);
		}

	}
	return (*tree)->symbol;
}
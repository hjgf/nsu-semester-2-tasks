#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "error.h"
#include "input.h"
#include "test.h"
#include "maketree.h"

#define DEBUG

int main(int argc, char ** argv)
{
	FILE * input;
	FILE * output;
	
#ifdef DEBUG
	FILE * fileTree = fopen("fileTree", "w");
#endif // DEBUG

	//Help
	if (argc == 1 || (argc == 2 && !strcmp("help", argv[1])))
	{
		fprintf(stdout, "Implementation of huffman archiver\n");
		fprintf(stdout, "By Roman Ivanov, 2014\n");
		fprintf(stdout, "Arguments: <input file> <output file> <direction>\n");
		fprintf(stdout, "Direction: \'c\' to compress, \'d\' to decompress\n");
		fprintf(stdout, "Output file may not exist\n");
		return 0;
	}
	
	if (4 != argc)
	{
		fprintf(stderr, "Error count of arguments");
		return ERROR_COUNT_ARGUMENTS;
	}
	if (strcmp(argv[3], "d") && strcmp(argv[3], "c"))
	{
		fprintf(stderr, "Error direcion");
		return ERROR_DIRECTION;
	}
	/***************************************/
	/*************COMPRESSION***************/
	/***************************************/

	if (4 == argc && !strcmp("c", argv[3]))
	{
		input = fopen(argv[1], "rb");
		if (NULL == input)
		{
			fprintf(stderr, "Input file hasn't been open");
			return ERROR_OPENING_FILE;
		}
		output = fopen(argv[2], "wb");
		if (NULL == input)
		{
			fprintf(stderr, "Output file hasn't been open");
			return ERROR_OPENING_FILE;
		}

		int totalCount = 0; //Count of symbols in source file

		//Getting table of freqs of different elements
		int* table = getSymbolTable(input);
		if (NULL == table)
		{
			fprintf(stderr, "Not enough memory");
			return NOT_ENOUGH_MEMORY;
		}

		for (int i = 0; i < MAX_SYMBOLS; i++)
		{
			totalCount += table[i];
		}

		if (0 == totalCount)
		{
			fprintf(stderr, "File is empty");
			return 0;
		}

#ifdef DEBUG
		for (int i = 0; i < 256; i++)
		{
			fprintf(fileTree, "%c ) %d\n", i, table[i]);
		}
#endif // DEBUG

		int counter = 0; //Count of different symbols

		//Building ordered list due to table of frequences
		s_list * list = buildList(table, &counter);
		if (NULL == list)
		{
			fprintf(stderr, "Not enough memory");
			return NOT_ENOUGH_MEMORY;
		}

#ifdef DEBUG
		printList(list, fileTree);
#endif // DEBUG


		free(table);

		//Building tree from the list
		if (0 == counter)
		{
			fprintf(stderr, "File is empty");
			return 0;
		}
		s_tree * tree;
		if (1 == counter)
		{
			tree = list->tree;
		}
		tree = bulldTree(&list, counter);
		if (NULL == tree)
		{
			fprintf(stderr, "Not enough memory");
			return NOT_ENOUGH_MEMORY;
		}

		//Calc codes for every node of the tree
		calcCodes(tree); 

		buffer * buf = initBuffer();
		if (NULL == buf)
		{
			fprintf(stderr, "Not enough memory");
			return NOT_ENOUGH_MEMORY;
		}

		//Building table of strings with Huffman codes(NULL if no such code)
		char ** code = extractCodes(tree);
		if (NULL == code)
		{
			fprintf(stderr, "Not enough memory");
			return NOT_ENOUGH_MEMORY;
		}

		//Writing tail, tree and every symbol to file
		zip(input, output, tree, code, buf);

		printf("Compression rate: %.2f%%", ((float)buf->totally / totalCount) * 100);

		freeTree(tree);
		free(code);
		free(buf);

		return 0;
	}

	/***************************************/
	/************DECOMPRESSION**************/
	/***************************************/
	if (4 == argc && !strcmp("d", argv[3]))
	{
		input = fopen(argv[1], "rb");
		if (NULL == input)
		{
			fprintf(stderr, "Input file hasn't been open");
			return ERROR_OPENING_FILE;
		}
		output = fopen(argv[2], "wb");
		if (NULL == input)
		{
			fprintf(stderr, "Output file hasn't been open");
			return ERROR_OPENING_FILE;
		}
		int tail; //Reading tail
		if (EOF == (tail = fgetc(input)))
		{
			fprintf(stderr, "Error reading file");
			return ERROR_READING_FILE;
		}

		//Initialyzing reading buffer
		buffer * buf = initBuffer();
		if (NULL == buf)
		{
			fprintf(stderr, "Not enough memory");
			return NOT_ENOUGH_MEMORY;
		}
		buf->nextByte = 0;
		if (EOF == (buf->nextByte = fgetc(input)))
		{
			fprintf(stderr, "Error reading file");
			return ERROR_READING_FILE;
		}

		//Rebuilding tree
		s_tree * tree = rebuildTree(input, buf, (char)tail);
		if (NULL == tree)
		{
			fprintf(stderr, "Error building tree");
			return ERROR_BUILDING_TREE;
		}

#ifdef DEBUG
		printTree(tree, fileTree);
#endif // DEBUG

		int tmp;

		//Decoding every symbol and writing to output file
		while (EOF != (tmp = decodeLetter(input, &tree, buf, (char)tail)))
		{
			if (ERROR_READING_FILE == tmp)
			{
				fprintf(stderr, "Error reading file");
				return ERROR_READING_FILE;
			}
			fputc((char)tmp, output);
		}

		free(buf);
		freeTree(tree);

		return 0;
	}
}
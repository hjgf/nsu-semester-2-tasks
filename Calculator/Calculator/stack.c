#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

Stack* createStack()
{
	Stack* stack;
	stack = (Stack*)malloc(sizeof(Stack));
	stack->top = NULL;
	return stack;
}

//just takes value from the top of the stack
int top(Stack *stack)
{
	if (stack->top)
	{
		return (stack->top->data);
	}
	else
	{
		return 0;
	}
}

//No check for emptiness
int pop(Stack *stack)
{
	int value;
	struct list *tmp;
	tmp = stack->top;
	value = tmp->data;
	stack->top = tmp->next;
	free(tmp);
	return value;
}

//No check for memory allocation
void push(Stack* stack, int value)
{
	struct list *tmp;
	tmp = (struct list*)malloc(sizeof(struct list));
	tmp->data = value;
	tmp->next = stack->top;
	stack->top = tmp;
}

//Check for emptiness
int empty(Stack *stack)
{
	return (stack->top == NULL);
}

//Frees memory of the stack
void deleteStack(Stack* stack)
{
	struct list *tmp;
	while (stack->top)
	{
		tmp = stack->top;
		stack->top = tmp->next;
		free(tmp);
	}
}

//For debug

void printList(FILE* output, struct list* list)
{
	if (NULL == list)
	{
		fprintf(output, "X");
		return;
	}
	fprintf(output, "%c -> ", list->data);
	printList(output, list->next);
}
#ifndef STACK_H
#define STACK_H

struct list
{
	int data;
	struct list* next;
};

typedef struct stack
{
	struct list *top;
} Stack;

Stack* createStack();
int top(Stack *stack);
int pop(Stack *stack);
void push(Stack* stack, int value);
int empty(Stack *stack);
void deleteStack(Stack* stack);

//For debug
void printList(FILE* output, struct list* list);

#endif // !STACK_C
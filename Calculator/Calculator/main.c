#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stack.h"
#include "calculator.h"

#define NOT_ENOUGH_MEMORY 1
#define NOT_OPEN_FILE 2
#define WRONG_INPUT 3
#define WRONG_BRACKETS_COUNT 4

void help()
{
	printf("This program implements calculator.\n");
	printf("Available operations: +, -, *, /. Also you can use brackets\n");
	printf("You can set variables(one letter) to expression\n");
	printf("Example input: (a + 8) * (12 / (b - 5))\n");
	printf("Arguments <input_file> <output_file>\n");
	printf("To the output file will be written infix form and result\n");
}

//Returns priority of operator
int getPriority(char ch)
{
	if (ch == '(')
		return 1;
	if (ch == ')')
		return 2;
	if (ch == '=')
		return 3;
	if ((ch == '+') || (ch == '-'))
		return 4;
	if ((ch == '*') || (ch == '/'))
		return 5;
	return 0;
}

//Check whether ch is variable(returns 1 if yes)
int isVariable(char ch)
{
	if (ch >= 'a' && ch <= 'z')
	{
		return 1;
	}
	return 0;
}

//Check whether ch is number(returns 1 if yes)
int isNumber(char ch)
{
	if (ch >= '0' && ch <= '9')
	{
		return 1;
	}
	return 0;
}

//Shell-function for handleOperator
//Used to process exceptions
int operation(Stack* stack, FILE* output, int operation)
{
	switch (handleOperator(stack, output, operation))
	{
		case EMPTY_STACK:
		{
							fprintf(stderr, "Error in expression. Check count of operators");
							return EMPTY_STACK;
		}
		case DIVISION_BY_ZERO:
		{
								 fprintf(stderr, "Forbidden operation. Division by zero");
								 return DIVISION_BY_ZERO;
		}
		case OVERFLOW:
		{
						 fprintf(stderr, "Overflow. Try to use smaller numbers");
						 return OVERFLOW;
		}
	}
	return 0;
}

//Function adds to the end of string char ch
void addCharToStr(char* string, char ch)
{
	int tmp = strlen(string);
	string[tmp] = ch;
	string[tmp + 1] = '\0';
}


int main(int argc, char** argv)
{
	if (1 == argc)
	{
		help();
		return 0;
	}
	if (argc == 2 && !strcmp(argv[1], "help"))
	{
		help();
		return 0;
	}
	if (3 != argc)
	{
		fprintf(stderr, "Wrong count of arguments");
		return 0;
	}


	FILE* input = fopen(argv[1], "r");
	if (NULL == input)
	{
		fprintf(stderr, "Failed to open input file");
		return 1;
	}

	FILE* output = fopen(argv[2], "w");
	if (NULL == output)
	{
		fprintf(stderr, "Failed to open output file");
		return 1;
	}

	Stack* stack = createStack();
	if (NULL == stack)
	{
		return NOT_ENOUGH_MEMORY;
	}
	Stack* numStack = createStack();
	if (NULL == numStack)
	{
		return NOT_ENOUGH_MEMORY;
	}

	char ch;
	char num[20] = "";
	int numIsEnded = 0;
	while (1 == fscanf(input, "%c", &ch))
	{
		if (isNumber(ch) || isVariable(ch) || getPriority(ch))
		{
			if (isNumber(ch))
			{
				//Accumulation of num
				fprintf(output, "%c", ch);
				addCharToStr(num, ch);
				numIsEnded = 1;
			}
			else
			{
				//Assigning a value
				if (isVariable(ch))
				{
					int tmp, value;
					do
					{
						fprintf(stdout, "Assign a value to  '%c'\n", ch);
						tmp = fscanf(stdin, "%d", &value);
						fflush(stdin);
					} while (1 != tmp);
					fprintf(output, "%d ", value);
					push(numStack, value);
					continue;
				}
				//Handling end of num
				if (numIsEnded)
				{
					fprintf(output, " ", ch);
					push(numStack, atoi(num));
					num[0] = '\0';
					numIsEnded = 0;
				}
				if (('(' == ch) || (')' == ch))
				{
					if ('(' == ch)
					{
						push(stack, ch);
					}
					if (')' == ch)
					{
						//Handling operations between brackets
						while ('(' != top(stack))
						{
							if (empty(stack))
							{
								fprintf(stderr, "Error in expression. Check count of brackets");
								return WRONG_BRACKETS_COUNT;
							}
							int tmp = pop(stack);
							fprintf(output, "%c ", tmp);
							operation(numStack, output, tmp);
						}
						pop(stack);
					}
				}
				//Operations handling
				else
				{
					while ((!empty(stack)) && (getPriority(top(stack)) >= getPriority(ch)))
					{
						int tmp = pop(stack);
						fprintf(output, "%c ", tmp);
						operation(numStack, output, tmp);
					}
					push(stack, ch);
				}
			}
		}
	}
	if (numIsEnded)
	{
		push(numStack, atoi(num));
		fprintf(output, " ");
		numIsEnded = 0;
	}
	//Handling left operations
	while (!empty(stack))
	{	
		int tmp = pop(stack);
		if ('(' == tmp)
		{
			fprintf(stderr, "Error in expression. Check count of brackets");
			return WRONG_BRACKETS_COUNT;
		}
		fprintf(output, "%c ", tmp);
		operation(numStack, output, tmp);
	}
	deleteStack(stack);
	deleteStack(numStack);
	return 0;
}
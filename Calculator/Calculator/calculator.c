#include <stdio.h>
#include <limits.h>
#include "stack.h"
#include "calculator.h"

//Processing operators. Executing action according to operator
//Returns error(non 0) if something is wrong
int handleOperator(Stack* stack, FILE* output, char operator)
{
	switch (operator)
	{
	case '=':
	{
				if (empty(stack))
				{
					return EMPTY_STACK;
				}
				fprintf(output, "%d\n", pop(stack));
				break;
	}
	case '+':	return addition(stack);
	case '-':	return subtraction(stack);
	case '*':	return multiplication(stack);
	case '/':	return division(stack);
	}
	return 0;
}

//After this comment appropirating functions for operator:

int addition(Stack* stack)
{
	int op1;
	int op2;
	if (empty(stack))
	{
		return EMPTY_STACK;
	}
	op1 = pop(stack);
	if (empty(stack))
	{
		return EMPTY_STACK;
	}
	op2 = pop(stack);
	//Check for overflow
	if (!(op1 < INT_MAX - op2 && op2 < INT_MAX - op1 &&
		op1 > -INT_MAX + op2 && op2 > -INT_MAX + op1))
	{
		return OVERFLOW;
	}
	push(stack, op1 + op2);
	return 0;
}

int subtraction(Stack* stack)
{
	int op1;
	int op2;
	if (empty(stack))
	{
		return EMPTY_STACK;
	}
	op1 = pop(stack);
	if (empty(stack))
	{
		return EMPTY_STACK;
	}
	op2 = pop(stack);
	if (!(op1 < INT_MAX - op2 && op2 < INT_MAX - op1 &&
		op1 > -INT_MAX + op2 && op2 > -INT_MAX + op1))
	{
		return OVERFLOW;
	}
	push(stack, -op1 + op2);
	return 0;
}

int multiplication(Stack* stack)
{
	int op1;
	int op2;
	if (empty(stack))
	{
		return EMPTY_STACK;
	}
	op1 = pop(stack);
	if (empty(stack))
	{
		return EMPTY_STACK;
	}
	op2 = pop(stack);
	if (!(op1 < INT_MAX / op2 && op2 < INT_MAX / op1 &&
		op1 > -INT_MAX / op2 && op2 > -INT_MAX / op1))
	{
		return OVERFLOW;
	}
	push(stack, op1 * op2);
	return 0;
}

int division(Stack* stack)
{
	int op1;
	int op2;
	if (empty(stack))
	{
		return EMPTY_STACK;
	}
	op1 = pop(stack);
	if (empty(stack))
	{
		return EMPTY_STACK;
	}
	op2 = pop(stack);
	if (0 == op1)
	{
		return DIVISION_BY_ZERO;
	}
	push(stack, op2 / op1);
	return 0;
}

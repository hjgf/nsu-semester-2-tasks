#ifndef CALCULATOR_H
#define CALCULATOR_H

#define EMPTY_STACK 1
#define OVERFLOW 2
#define DIVISION_BY_ZERO 3

int handleOperator(Stack* stack, FILE* output, char operator);
int addition(Stack* stack);
int subtraction(Stack* stack);
int multiplication(Stack* stack);
int division(Stack* stack);

#endif // !CALCULATOR_H

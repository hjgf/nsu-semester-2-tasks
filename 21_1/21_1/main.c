#include <stdio.h>
#include <stdlib.h>

//#pragma comment(linker, "/STACK:100000000000")

#define NO_MEMORY 1
#define ERROR_READING_FILE 2
#define ERROR_OPENING_FILE 3

#define DEBUG

typedef struct List
{
	struct List* previous;
	int x;
	int y;
	int dist;
} List;

typedef struct Queue
{
	struct List* last;
	struct List* first;
	int count;
} Queue;

List* createList(int x, int y, int dist)
{
	List* tmp;
	if (NULL == (tmp = (List*)malloc(sizeof(List))))
	{
		return NULL;
	}
	tmp->x = x;
	tmp->y = y;
	tmp->dist = dist;
	tmp->previous = NULL;
	return tmp;
}

Queue* createQueue()
{
	Queue* tmp;
	if (NULL == (tmp = (Queue*)malloc(sizeof(Queue))))
	{
		return NULL;
	}
	tmp->first = NULL;
	tmp->last = NULL;
	tmp->count = 0;
	return tmp;
}

//Returns non-0 if error
int addToQueue(Queue* queue, int x, int y, int dist)
{
	List* tmp;
	if (NULL == (tmp = createList(x, y, dist)))
	{
		return 1;
	}
	if (NULL == queue->first)
	{
		queue->first = tmp;
		queue->last = tmp;
		return 0;
	}
	queue->last->previous = tmp;
	queue->last = tmp;
	queue->count++;
	return 0;
}

void printList(FILE* output, List* head)
{
	if (NULL == head)
	{
		return;
	}
	fprintf(output, "(%d; %d) d = %d <- ", head->x, head->y, head->dist);
	printList(output, head->previous);
	return;
}

void getFromQueue(Queue* queue, int* x, int* y, int* dist)
{
	*x = queue->first->x;
	*y = queue->first->y;
	*dist = queue->first->dist;
	List* ptr = queue->first;
	queue->first = queue->first->previous;
	free(ptr);
	queue->count--;
}

void deleteList(List* head)
{
	if (NULL == head)
	{
		return;
	}
	deleteList(head->previous);
	free(head);
}

void deleteQueue(Queue* queue)
{
	deleteList(queue->first);
	free(queue);
}

int isEmpty(Queue* queue)
{
	if (NULL == queue->first)
	{
		return 1;
	}
	return 0;
}



void breadthFirstSearch(int** map, Queue* queue, int height, int width, int* dist)
{
	int x, y, d;
	while (!isEmpty(queue))
	{
		getFromQueue(queue, &x, &y, &d);
		map[y][x] = 'P';
		if (y > 0)
		{
			if ('F' == map[y - 1][x])
			{
				*dist = d + 1;
				return;
			}
			if ('.' == map[y - 1][x])
			{
				map[y - 1][x] = 'Q';
				addToQueue(queue, x, y - 1, d + 1);
			}
		
		}
		if (y < height - 1)
		{
			if ('F' == map[y + 1][x])
			{
				*dist = d + 1;
				return;
			}
			if ('.' == map[y + 1][x])
			{
				map[y + 1][x] = 'Q';
				addToQueue(queue, x, y + 1, d + 1);
			}
		}
		if (x > 0)
		{
			if ('F' == map[y][x - 1])
			{
				*dist = d + 1;
				return;
			}
			if ('.' == map[y][x - 1])
			{
				map[y][x - 1] = 'Q';
				addToQueue(queue, x - 1, y, d + 1);
			}
		}
		if (x < width - 1)
		{
			if ('F' == map[y][x + 1])
			{
				*dist = d + 1;
				return;
			}
			if ('.' == map[y][x + 1])
			{
				map[y][x + 1] = 'Q';
				addToQueue(queue, x + 1, y, d + 1);
			}
		}
	}
}



int main()
{
	FILE* input = fopen("input.txt", "r");
	if (NULL == input)
	{
		fprintf(stderr, "Failed to open input file");
		return ERROR_OPENING_FILE;
	}

	FILE* output = fopen("output.txt", "w");
	if (NULL == output)
	{
		fprintf(stderr, "Failed to open output file");
		return ERROR_OPENING_FILE;
	}

	int height;
	int width;
	if (2 != fscanf(input, "%d %d", &height, &width))
	{
		fprintf(stderr, "Error reading file");
		return ERROR_READING_FILE;
	}

	int** map;
	if (NULL == (map = (int**)malloc(sizeof(int*)* height)))
	{
		fprintf(stderr, "Not enough memory");
		return NO_MEMORY;
	}
	for (int i = 0; i <= height; i++)
	{
		if (NULL == (map[i] = (int*)malloc(sizeof(int)* width)))
		{
			fprintf(stderr, "Not enough memory");
			return NO_MEMORY;
		}
	}
	int xStart = -1;
	int yStart = -1;

	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			int tmp = fgetc(input);
			while ('\n' == tmp)
			{
				tmp = fgetc(input);
			}
			if ('S' == tmp)
			{
				xStart = j;
				yStart = i;
				tmp = '0';
			}
			map[i][j] = tmp;
		}
	}

	int dist = -1;

	Queue* queue;
	if (NULL == (queue = createQueue()))
	{
		fprintf(stderr, "Not enough memory");
		return NO_MEMORY;
	}
	addToQueue(queue, xStart, yStart, 0);
	breadthFirstSearch(map, queue, height, width, &dist);
	fprintf(output, "%d", dist);

	//Memory deallocation
	/*deleteQueue(queue);
	for (int i = 0; i < height; i++)
	{
		free(map[i]);
	}*/

#ifdef DEBUG
	fputc('\n', output);
	fprintf(output, "s = (%d;%d)\n", xStart, yStart);
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			fprintf(output, "%�", map[i][j]);
		}
		fputc('\n', output);
	}
	fputc('\n', output);
#endif // DEBUG
	return 0;
}
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <assert.h>

//#pragma comment(linker, "/STACK:100000000000")

#define NO_MEMORY 1
#define ERROR_READING_FILE 2
#define ERROR_OPENING_FILE 3

#define NO_MEMORY_EXCEPTION {fprintf(stderr, "Not enough memory"); return NO_MEMORY;}
#define ERR_READING_FILE_EXCEPTION 	{fprintf(stderr, "Error reading file");return ERROR_READING_FILE;}
#define ERR_OPENING_FILE_EXCEPTION 	{fprintf(stderr, "Error opening file");return ERROR_OPENING_FILE;}

#define DEBUG

int main()
{
	FILE* input = fopen("input.txt", "r");
	if (NULL == input)
		ERR_OPENING_FILE_EXCEPTION

	FILE* output = fopen("output.txt", "w");
	if (NULL == output)
		ERR_OPENING_FILE_EXCEPTION

	int height;
	int width;
	if (2 != fscanf(input, "%d %d", &height, &width))
		ERR_READING_FILE_EXCEPTION

	int** map;
	if (NULL == (map = (int**)malloc(sizeof(int*)* height)))
		NO_MEMORY_EXCEPTION
	for (int i = 0; i < height; i++)
	{
		if (NULL == (map[i] = (int*)malloc(sizeof(int)* width)))
			NO_MEMORY_EXCEPTION
	}
	return 0;
}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NOT_ENOUGH_MEMORY 1
#define NOT_OPEN_FILE 2
#define WRONG_INPUT 3

//Stack realisation
struct list 
{
	char data;
	struct list* next;
};

typedef struct stack 
{
	struct list *top; 
} Stack;

Stack* createStack()
{
	Stack* stack;
	stack = (Stack*)malloc(sizeof(Stack));
	stack->top = NULL;
	return stack;
}

char top(Stack *stack)
{
	if (stack->top)
	{
		return (stack->top->data);
	}
	else
	{
		return 0;
	}
}

char pop(Stack *stack)
{
	char value;
	struct list *tmp;
	tmp = stack->top;
	value = tmp->data;
	stack->top = tmp->next;
	free(tmp);
	return value;
}

void push(Stack* stack, char value)
{
	struct list *tmp;
	tmp = (struct list *) malloc(sizeof (struct list));
	tmp->data = value;
	tmp->next = stack->top;
	stack->top = tmp;
}

int empty(Stack *stack)
{
	return (stack->top == NULL);
}

void deleteStack(Stack* stack)
{
	struct list *tmp;
	while (stack->top)
	{
		tmp = stack->top;
		stack->top = tmp->next;
		free(tmp);
	}
}

int getPriority(char ch)
{
	if (ch == '(')
		return 1;
	if (ch == ')')
		return 2;
	if (ch == '=')
		return 3;
	if ((ch == '+') || (ch == '-'))
		return 4;
	if ((ch == '*') || (ch == '/'))
		return 5;
	return 0;
}

int isOperand(char ch)
{
	if (((ch >= '0') && (ch <= '9')) || ((ch >= 'a') && (ch <= 'z')))
	{
		return 1;
	}

	return 0;
}

int main()
{
	FILE* input = fopen("input.txt", "r");
	if (NULL == input)
	{
		fprintf(stderr, "Failed to open input file");
		return 1;
	}

	FILE* output = fopen("output.txt", "w");
	if (NULL == output)
	{
		fprintf(stderr, "Failed to open output file");
		return 1;
	}

	Stack* stack = createStack();
	if (NULL == stack)
	{
		return NOT_ENOUGH_MEMORY;
	}

	char ch , a;

	int wordIsEnded = 0;
	while (fscanf(input, "%c", &a) == 1)
	{
		if (isOperand(a) || getPriority(a))
		{
			if (isOperand(a))
			{
				fprintf(output, "%c", a);
				wordIsEnded = 1;
			}
			else
			{
				if (wordIsEnded)
				{
					fprintf(output, " ");
					wordIsEnded = 0;
				}
				if ((a == '(') || (a == ')'))
				{
					if (a == '(')
					{
						push(stack, a);
					}
					if (a == ')')
					{
						while (top(stack) != '(')
						{
							fprintf(output, "%c ", pop(stack));
						}
						pop(stack);
					}
				}
				else
				{
					while ((!empty(stack)) && (getPriority(top(stack)) >= getPriority(a)))
					{
						fprintf(output, "%c ", pop(stack));
					}
					push(stack, a);
				}
			}
		}
	}
	if (wordIsEnded)
	{
		fprintf(output, " ");
		wordIsEnded = 0;
	}

	/*
	while ((top(stack) == '\n') || (top(stack) == ' '))
	{	
		pop(stack);
	}*/
	
	//Printing left operations
	while (!empty(stack))
	{
		fprintf(output, "%c ", pop(stack));
	}
	return 0;
}
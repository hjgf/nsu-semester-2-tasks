#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

//#pragma comment(linker, "/STACK:100000000000")

#define NO_MEMORY 1
#define ERROR_READING_FILE 2
#define ERROR_OPENING_FILE 3

#define DEBUG

typedef enum direction
{
	up = 1, left = -2, down = -1, right = 2
} direction;

typedef struct List
{
	struct List* previous;
	int x;
	int y;
	int dist;
} List;

typedef struct Queue
{
	struct List* last;
	struct List* first;
	int count;
} Queue;

List* createList(int x, int y, int dist)
{
	List* tmp;
	if (NULL == (tmp = (List*)malloc(sizeof(List))))
	{
		return NULL;
	}
	tmp->x = x;
	tmp->y = y;
	tmp->dist = dist;
	tmp->previous = NULL;
	return tmp;
}

Queue* createQueue()
{
	Queue* tmp;
	if (NULL == (tmp = (Queue*)malloc(sizeof(Queue))))
	{
		return NULL;
	}
	tmp->first = NULL;
	tmp->last = NULL;
	tmp->count = 0;
	return tmp;
}

//Returns non-0 if error
int addToQueue(Queue* queue, int x, int y, int dist)
{
	List* tmp;
	if (NULL == (tmp = createList(x, y, dist)))
	{
		return 1;
	}
	if (NULL == queue->first)
	{
		queue->first = tmp;
		queue->last = tmp;
		return 0;
	}
	queue->last->previous = tmp;
	queue->last = tmp;
	queue->count++;
	return 0;
}

void printList(FILE* output, List* head)
{
	if (NULL == head)
	{
		return;
	}
	fprintf(output, "(%d; %d) d = %d <- ", head->x, head->y, head->dist);
	printList(output, head->previous);
	return;
}

void getFromQueue(Queue* queue, int* x, int* y, int* dist)
{
	*x = queue->first->x;
	*y = queue->first->y;
	*dist = queue->first->dist;
	List* ptr = queue->first;
	queue->first = queue->first->previous;
	free(ptr);
	queue->count--;
}

void deleteList(List* head)
{
	if (NULL == head)
	{
		return;
	}
	deleteList(head->previous);
	free(head);
}

void deleteQueue(Queue* queue)
{
	deleteList(queue->first);
	free(queue);
}

int isEmpty(Queue* queue)
{
	if (NULL == queue->first)
	{
		return 1;
	}
	return 0;
}

void breadthFirstSearch(int** map, Queue* queue, int height, int width, int* dist)
{
	int x, y, d;
	while (!isEmpty(queue))
	{
		getFromQueue(queue, &x, &y, &d);
		map[y][x] = d;
		if (y > 0)
		{
			if ('T' == map[y - 1][x])
			{
				*dist = d + 1;
				return;
			}
			if ('.' == map[y - 1][x])
			{
				map[y - 1][x] = 'Q';
				addToQueue(queue, x, y - 1, d + 1);
			}
		}
		if (y < height - 1)
		{
			if ('T' == map[y + 1][x])
			{
				*dist = d + 1;
				return;
			}
			if ('.' == map[y + 1][x])
			{
				map[y + 1][x] = 'Q';
				addToQueue(queue, x, y + 1, d + 1);
			}
		}
		if (x > 0)
		{
			if ('T' == map[y][x - 1])
			{
				*dist = d + 1;
				return;
			}
			if ('.' == map[y][x - 1])
			{
				map[y][x - 1] = 'Q';
				addToQueue(queue, x - 1, y, d + 1);
			}
		}
		if (x < width - 1)
		{
			if ('T' == map[y][x + 1])
			{
				*dist = d + 1;
				return;
			}
			if ('.' == map[y][x + 1])
			{
				map[y][x + 1] = 'Q';
				addToQueue(queue, x + 1, y, d + 1);
			}
		}
	}
}

//returns count of rotaions(below zero means to the left)
int rotateToDirection(direction initDir, direction destDir)
{
	if (initDir == destDir)
	{
		return 0;
	}
	if (initDir == -destDir)
	{
		return 2;
	}
	if (initDir == right || initDir == down)
	{
		if (destDir < 0)
		{
			return -1;
		}
		return 1;
	}
	if (destDir > 0)
	{
		return -1;
	}
	return 1;
}

//Add border processing
void findPath(int** map, int x, int y, int* newX, int* newY, int height, int width)
{
	if (x > 0 && ('#' != map[y][x - 1]) && (map[y][x - 1] == map[y][x] - 1))
	{
		*newX = x - 1;
		*newY = y;
	}
	if (y > 0 && ('#' != map[y - 1][x]) && (map[y - 1][x] == map[y][x] - 1))
	{
		*newX = x;
		*newY = y - 1;
	}
	if (y < height - 1 && ('#' != map[y + 1][x]) && (map[y + 1][x] == map[y][x] - 1))
	{
		*newX = x;
		*newY = y + 1;
	}
	if (y < height - 1 && ('#' != map[y][x + 1]) && (map[y][x + 1] == map[y][x] - 1))
	{
		*newX = x + 1;
		*newY = y;
	}
}

direction getDirection(int x1, int y1, int x2, int y2)
{
	if (x1 > x2)
	{
		return left;
	}
	if (y1 > y2)
	{
		return up;
	}
	if (y1 < y2)
	{
		return down;
	}
	return right;
}

void addToArray(char* array, char value, int* length, int currentLength)
{
	if (*length <= currentLength + 1)
	{
		realloc(array, sizeof(int)* (*length) * 2 + 1);
		*length = *length * 2;
	}
	array[currentLength] = value;
}

int main()
{
	FILE* input = fopen("input.txt", "r");
	if (NULL == input)
	{
		fprintf(stderr, "Failed to open input file");
		return ERROR_OPENING_FILE;
	}

	FILE* output = fopen("output.txt", "w");
	if (NULL == output)
	{
		fprintf(stderr, "Failed to open output file");
		return ERROR_OPENING_FILE;
	}

	int height;
	int width;
	if (2 != fscanf(input, "%d %d", &height, &width))
	{
		fprintf(stderr, "Error reading file");
		return ERROR_READING_FILE;
	}

	int** map;
	if (NULL == (map = (int**)malloc(sizeof(int*)* height)))
	{
		fprintf(stderr, "Not enough memory");
		return NO_MEMORY;
	}
	for (int i = 0; i <= height; i++)
	{
		if (NULL == (map[i] = (int*)malloc(sizeof(int)* width)))
		{
			fprintf(stderr, "Not enough memory");
			return NO_MEMORY;
		}
	}
	int xStart = -1;
	int yStart = -1;
	int xFinish = -1;
	int yFinish = -1;

	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			int tmp = fgetc(input);
			while ('\n' == tmp)
			{
				tmp = fgetc(input);
			}
			if ('S' == tmp)
			{
				xStart = j;
				yStart = i;
				tmp = '0';
			}
			if ('T' == tmp)
			{
				xFinish = j;
				yFinish = i;
			}
			if ('#' == tmp)
			{
				tmp = 88; // TODO intmax
			}
			map[i][j] = tmp;
		}
	}

	int dist = -1;

	Queue* queue;
	if (NULL == (queue = createQueue()))
	{
		fprintf(stderr, "Not enough memory");
		return NO_MEMORY;
	}
	addToQueue(queue, xStart, yStart, 0);
	breadthFirstSearch(map, queue, height, width, &dist);

	deleteQueue(queue);

	//Now we make backtrace
	int actionsCount;
	/*Queue* backtrace = NULL;
	if (NULL == (backtrace = createQueue()))
	{
	return NO_MEMORY;
	}*/
	direction initDir = up;
	direction newDir;
	direction oldDir;
	int currX = xFinish;
	int currY = yFinish;
	int newX;
	int newY;

	map[yFinish][xFinish] = dist;

	findPath(map, currX, currY, &newX, &newY, height, width);
	oldDir = getDirection(newX, newY, currX, currY);
	currX = newX;
	currY = newY;
	do
	{
		findPath(map, currX, currY, &newX, &newY, height, width);
		newDir = getDirection(newX, newY, currX, currY);
		switch (rotateToDirection(newDir, oldDir))
		{
		case 0:
		{
				  fputc('F', output);
				  break;
		}
		case 1:
		{
				  fprintf(output, "LF");
				 // actionsCount++;
				  break;
		}
		case -1:
		{
				   fprintf(output, "RF");
				   //actionsCount++;
				   break;
		}
		case 2:
		{
				  fprintf(output, "LLF");
				  //actionsCount += 2;
				  break;
		}
		}
		currX = newX;
		currY = newY;
		//addToQueue(backtrace, currX, currY, 0);
	} while (newX != xStart || newY != yStart);
	//actionsCount = backtrace->count - 1;

#ifdef DEBUG
	//fprintf(output, "%d %d dest point; %d - transloc\n", newX, newY, actionsCount);
	//printList(output, backtrace->first);
#endif // DEBUG

	//Now we have a path, so we just go along it and write rotations
	/*currX = xStart;
	currY = yStart;

	//getFromQueue(backtrace, &newX, &newY, &dist);
	while (!isEmpty(backtrace))
	{
	getFromQueue(backtrace, &newX, &newY, &dist);
	newDir = getDirection(currX, currY, newX, newY);

	switch (rotateToDirection(initDir, newDir))
	{
	case 0:
	{
	fputc('F', output);
	break;
	}
	case 1:
	{
	fprintf(output, "LF");
	actionsCount++;
	break;
	}
	case -1:
	{
	fprintf(output, "RF");
	actionsCount++;
	break;
	}
	case 2:
	{
	fprintf(output, "LLF");
	actionsCount += 2;
	break;
	}

	default:
	break;
	}
	currX = newX;
	currY = newY;
	}
	*/
#ifdef DEBUG
	fputc('\n', output);
	fprintf(output, "s = (%d;%d)\n", xStart, yStart);
	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++)
		{
			fprintf(output, "%.2d ", map[i][j]);
		}
		fputc('\n', output);
	}
	fputc('\n', output);
#endif // DEBUG

	//Memory deallocation
	/*deleteQueue(backtrace);
	for (int i = 0; i < height; i++)
	{
	free(map[i]);
	}*/
	return 0;
}
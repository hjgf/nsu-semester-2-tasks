#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

//#pragma comment(linker, "/STACK:100000000000")

#define NO_MEMORY 1
#define ERROR_READING_FILE 2
#define ERROR_OPENING_FILE 3

#define INFINITY 1000001

//#define DEBUG

int main(void)
{
	FILE* input = fopen("input.txt", "r");
	if (NULL == input)
	{
		fprintf(stderr, "Failed to open input file");
		return ERROR_OPENING_FILE;
	}

	FILE* output = fopen("output.txt", "w");
	if (NULL == output)
	{
		fprintf(stderr, "Failed to open output file");
		return ERROR_OPENING_FILE;
	}

	int places;
	int roads;
	if (2 != fscanf(input, "%d %d", &places, &roads))
	{
		fprintf(stderr, "Error reading file");
		return ERROR_READING_FILE;
	}

	int** matrix; //Matrix of minimal times between two places
	if (NULL == (matrix = (int**)malloc(sizeof(int*)* places)))
	{
		fprintf(stderr, "Not enough memory");
		return NO_MEMORY;
	}
	for (int i = 0; i < places; i++)
	{
		if (NULL == (matrix[i] = (int*)calloc(places, sizeof(int))))
		{
			fprintf(stderr, "Not enough memory");
			return NO_MEMORY;
		}
	}

	//Building matrix
	for (int i = 0; i < places; i++)
	{
		for (int j = 0; j < places; j++)
		{
			matrix[i][j] = INFINITY;
		}
	}

	for (int i = 0; i < places; i++)
	{
		matrix[i][i] = 0;
	}

	int place1;
	int place2;
	int time;
	for (int i = 0; i < roads; i++)
	{
		if (3 != fscanf(input, "%d %d %d", &place1, &place2, &time))
		{
			fprintf(stderr, "Error reading file");
			return ERROR_READING_FILE;
		}
		--place1;
		--place2;
		matrix[place1][place2] = min(time, matrix[place1][place2]);
		matrix[place2][place1] = min(time, matrix[place2][place1]);
	}

	for (int k = 0; k < places; k++)
	{
		for (int i = 0; i < places; i++)
		{
			for (int j = 0; j < places; j++)
			{
				matrix[i][j] = min(matrix[i][j], matrix[i][k] + matrix[k][j]);
			}
		}
	}
	
	int orders;
	if (1 != fscanf(input, "%d", &orders))
	{
		fprintf(stderr, "Error reading file");
		return ERROR_READING_FILE;
	}

	//Orders processing	
	int startPoint;
	int endPoint;
	for (int i = 0; i < orders; i++)
	{
		if (2 != fscanf(input, "%d %d", &startPoint, &endPoint))
		{
			fprintf(stderr, "Error reading file");
			return ERROR_READING_FILE;
		}
		fprintf(output, "%d\n", matrix[startPoint - 1][endPoint - 1]);
	}

#ifdef DEBUG
	for (int i = 0; i < places; i++)
	{
		for (int j = 0; j < places; j++)
		{
			if (matrix[i][j] == INFINITY)
			{
				fprintf(output, "i ");
				continue;
			}
			fprintf(output, "%d ", matrix[i][j]);
		}
		fprintf(output, "\n");
	}
#endif // DEBUG

	return 0;
}